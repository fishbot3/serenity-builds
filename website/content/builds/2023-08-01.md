---
title: 'SerenityOS build: Tuesday, August 01'
date: 2023-08-01T05:07:51Z
author: Buildbot
description: |
  Commit: cd0fe4bb48 (Kernel: Mark sys$poll as not needing the big lock, 2023-07-03)

  - [serenity-x86_64-20230801-cd0fe4b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230801-cd0fe4b.img.gz) (Raw image, 188.42 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230801-cd0fe4b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230801-cd0fe4b.img.gz) (Raw image, 188.42 MiB)

### Last commit :star:

```git
commit cd0fe4bb48f3c87ce7ec1397e79b892e352ac8f1
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Mon Jul 3 11:35:46 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Aug 1 05:35:26 2023 +0200

    Kernel: Mark sys$poll as not needing the big lock
```
