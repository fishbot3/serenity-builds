---
title: 'SerenityOS build: Wednesday, July 13'
date: 2022-07-13T03:05:22Z
author: Buildbot
description: |
  Commit: d2b887a793 (LibWeb: Only create one wrapper for inline content inside flex container, 2022-07-13)

  - [serenity-i686-20220713-d2b887a.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220713-d2b887a.img.gz) (Raw image, 126.82 MiB)
  - [serenity-i686-20220713-d2b887a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220713-d2b887a.vdi.gz) (VirtualBox VDI, 126.63 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220713-d2b887a.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220713-d2b887a.img.gz) (Raw image, 126.82 MiB)
- [serenity-i686-20220713-d2b887a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220713-d2b887a.vdi.gz) (VirtualBox VDI, 126.63 MiB)

### Last commit :star:

```git
commit d2b887a793397a7a22c7ba50e7f6cb704f809446
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Wed Jul 13 01:18:59 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jul 13 01:26:29 2022 +0200

    LibWeb: Only create one wrapper for inline content inside flex container
    
    Due to a missing `return` statement, we were creating two anonymous
    wrapper blocks around each piece of inline content inside a flex
    container.
    
    This had no visual impact, since they ended up with 0x0 dimensions,
    but we were wasting a fair bit of time running layout on them.
```
