---
title: 'SerenityOS build: Wednesday, April 05'
date: 2023-04-05T05:02:28Z
author: Buildbot
description: |
  Commit: 69e8216f2c (LibWeb: Do not use OS error codes in the error callback for file:// URLs, 2023-04-04)

  - [serenity-x86_64-20230405-69e8216.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230405-69e8216.img.gz) (Raw image, 174.96 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230405-69e8216.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230405-69e8216.img.gz) (Raw image, 174.96 MiB)

### Last commit :star:

```git
commit 69e8216f2c5310ca68409ecaf501266da43f14ba
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Tue Apr 4 16:12:42 2023 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Apr 4 22:41:20 2023 +0100

    LibWeb: Do not use OS error codes in the error callback for file:// URLs
    
    The error code passed here is expected to be an HTTP error code. Passing
    errno codes does not make sense in that context.
```
