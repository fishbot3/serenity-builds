---
title: 'SerenityOS build: Tuesday, June 07'
date: 2022-06-07T14:58:17Z
author: Buildbot
description: |
  Commit: 94bb5a779b (LibWeb: Use `long long` where it was replaced with `long`, 2022-05-10)

  - [serenity-i686-20220607-94bb5a7.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220607-94bb5a7.img.gz) (Raw image, 123.64 MiB)
  - [serenity-i686-20220607-94bb5a7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220607-94bb5a7.vdi.gz) (VirtualBox VDI, 123.46 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220607-94bb5a7.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220607-94bb5a7.img.gz) (Raw image, 123.64 MiB)
- [serenity-i686-20220607-94bb5a7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220607-94bb5a7.vdi.gz) (VirtualBox VDI, 123.46 MiB)

### Last commit :star:

```git
commit 94bb5a779b47ca20502e0f53154758eede33b4a4
Author:     stelar7 <dudedbz@gmail.com>
AuthorDate: Tue May 10 11:08:02 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Jun 6 22:34:45 2022 +0100

    LibWeb: Use `long long` where it was replaced with `long`
```
