---
title: 'SerenityOS build: Thursday, June 22'
date: 2023-06-22T05:04:27Z
author: Buildbot
description: |
  Commit: 4ed7456486 (Ping: Add TTL config option and value to output, 2023-06-19)

  - [serenity-x86_64-20230622-4ed7456.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230622-4ed7456.img.gz) (Raw image, 184.96 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230622-4ed7456.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230622-4ed7456.img.gz) (Raw image, 184.96 MiB)

### Last commit :star:

```git
commit 4ed74564864f1f1c44ced5078c780ba5fda74437
Author:     Optimoos <dmarkd@gmail.com>
AuthorDate: Mon Jun 19 14:59:26 2023 -0400
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Thu Jun 22 00:14:56 2023 +0200

    Ping: Add TTL config option and value to output
    
    This change adds the TTL value of the inbound packet to the output of
    the userland ping program, bringing it more in line with other common
    ping utilities. It also adds the (optional) -t option to configure the
    TTL of the outgoing packet if desired.
```
