---
title: 'SerenityOS build: Friday, November 10'
date: 2023-11-10T06:10:48Z
author: Buildbot
description: |
  Commit: 7f501d4d8a (Taskbar/QuickLaunchWidget: Ensure config backwards compatibility, 2023-11-05)

  - [serenity-x86_64-20231110-7f501d4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231110-7f501d4.img.gz) (Raw image, 202.45 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231110-7f501d4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231110-7f501d4.img.gz) (Raw image, 202.45 MiB)

### Last commit :star:

```git
commit 7f501d4d8a3cb35a24fd5412bc096a12385b4713
Author:     david072 <david.g.ganz@gmail.com>
AuthorDate: Sun Nov 5 12:49:52 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Nov 9 23:35:52 2023 +0100

    Taskbar/QuickLaunchWidget: Ensure config backwards compatibility
    
    The QuickLaunchWidget can now also parse the old config format, so that
    we stay compatible with the old format. After loading, it deletes the
    old config values and saves them in the new format.
```
