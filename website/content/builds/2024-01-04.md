---
title: 'SerenityOS build: Thursday, January 04'
date: 2024-01-04T06:11:35Z
author: Buildbot
description: |
  Commit: fa24fbf120 (LibGfx/OpenType: Survive simple glyphs with 0 contours, 2024-01-03)

  - [serenity-x86_64-20240104-fa24fbf.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240104-fa24fbf.img.gz) (Raw image, 206.61 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240104-fa24fbf.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240104-fa24fbf.img.gz) (Raw image, 206.61 MiB)

### Last commit :star:

```git
commit fa24fbf1203131558c83f1baf043dc0bf3e600b7
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Wed Jan 3 20:42:45 2024 -0500
Commit:     Alexander Kalenik <kalenik.aliaksandr@gmail.com>
CommitDate: Thu Jan 4 03:32:46 2024 +0100

    LibGfx/OpenType: Survive simple glyphs with 0 contours
    
    These are valid per spec, and do sometimes occur in practice, e.g.
    in embedded fonts in 0000550.pdf and 0000246.pdf in 0000.zip in the
    PDFA test set.
```
