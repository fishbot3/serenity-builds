---
title: 'SerenityOS build: Thursday, August 25'
date: 2022-08-25T03:07:23Z
author: Buildbot
description: |
  Commit: 8b8cee3172 (LibCrypto: Implement a (mostly) proper to_double for UnsignedBigInteger, 2022-08-24)

  - [serenity-i686-20220825-8b8cee3.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220825-8b8cee3.img.gz) (Raw image, 129.7 MiB)
  - [serenity-i686-20220825-8b8cee3.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220825-8b8cee3.vdi.gz) (VirtualBox VDI, 129.52 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220825-8b8cee3.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220825-8b8cee3.img.gz) (Raw image, 129.7 MiB)
- [serenity-i686-20220825-8b8cee3.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220825-8b8cee3.vdi.gz) (VirtualBox VDI, 129.52 MiB)

### Last commit :star:

```git
commit 8b8cee31728b02a500ee530e081c7b767ebb68cd
Author:     davidot <davidot@serenityos.org>
AuthorDate: Wed Aug 24 10:13:16 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Wed Aug 24 23:27:17 2022 +0100

    LibCrypto: Implement a (mostly) proper to_double for UnsignedBigInteger
    
    SignedBigInteger can immediately use this by just negating the double if
    the sign bit is set.
    For simple cases (below 2^53) we can just convert via an u64, however
    above that we need to extract the top 53 bits and use those as the
    mantissa.
    
    This function currently does not behave exactly as the JS spec specifies
    however it is much less naive than the previous implementation.
```
