---
title: 'SerenityOS build: Thursday, March 02'
date: 2023-03-02T06:01:47Z
author: Buildbot
description: |
  Commit: 6b849fc8b1 (Kernel: Move TYPEDEF_* TTY macros to API/ttydefaults.h file, 2023-02-26)

  - [serenity-x86_64-20230302-6b849fc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230302-6b849fc.img.gz) (Raw image, 165.87 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230302-6b849fc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230302-6b849fc.img.gz) (Raw image, 165.87 MiB)

### Last commit :star:

```git
commit 6b849fc8b1c51772359c4202df274890284ba4ff
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Sun Feb 26 18:56:25 2023 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Wed Mar 1 19:36:53 2023 -0700

    Kernel: Move TYPEDEF_* TTY macros to API/ttydefaults.h file
    
    This allows us to get rid of an include to LibC/sys/ttydefaults.h in the
    Kernel TTY implementation.
    
    Also, move ttydefchars static const struct to another file called
    Kernel/API/ttydefaultschars.h, so it could be used too in the Kernel TTY
    implementation without the need to include anything from LibC.
```
