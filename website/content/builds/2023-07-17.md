---
title: 'SerenityOS build: Monday, July 17'
date: 2023-07-17T05:07:48Z
author: Buildbot
description: |
  Commit: 5c2c412318 (Meta: Bind WritableStreamDefaultController to JS, 2023-07-16)

  - [serenity-x86_64-20230717-5c2c412.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230717-5c2c412.img.gz) (Raw image, 187.76 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230717-5c2c412.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230717-5c2c412.img.gz) (Raw image, 187.76 MiB)

### Last commit :star:

```git
commit 5c2c41231853586a1ba7e7eb141804c2e1e3c231
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Sun Jul 16 09:48:05 2023 -0600
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Jul 17 00:00:49 2023 +0100

    Meta: Bind WritableStreamDefaultController to JS
    
    This was missed in 868cd9506923b6e47b56fdb0ec7cb1cc09031960.
```
