---
title: 'SerenityOS build: Sunday, March 12'
date: 2023-03-12T06:02:10Z
author: Buildbot
description: |
  Commit: 3cff36b7ab (Meta+CMake: Remove "image" ninja target in favor of "qemu-image", 2023-03-11)

  - [serenity-x86_64-20230312-3cff36b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230312-3cff36b.img.gz) (Raw image, 166.78 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230312-3cff36b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230312-3cff36b.img.gz) (Raw image, 166.78 MiB)

### Last commit :star:

```git
commit 3cff36b7ab427e040c9cac80a41cde9cfa0ac75d
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Mar 11 20:12:37 2023 -0500
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Mar 12 01:48:56 2023 +0000

    Meta+CMake: Remove "image" ninja target in favor of "qemu-image"
    
    "image" was an alias for "qemu-image".
    
    I want to add an `image` userland utility, which clashes with that
    shortname.
    
    So remove the existing "image" target. It was just an alias for
    "qemu-image".
    
    If you use serenity.sh to build, nothing changes. This only affects you
    if you run ninja manually -- you now have to say `ninja qemu-image` to
    build the disk image.
```
