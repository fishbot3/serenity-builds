---
title: 'SerenityOS build: Sunday, October 16'
date: 2022-10-16T18:34:37Z
author: Buildbot
description: |
  Commit: 0ea4d228e6 (LibWeb: Add missing body has_value() check in XMLHttpRequest::send(), 2022-10-16)

  - [serenity-x86_64-20221016-0ea4d22.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221016-0ea4d22.vdi.gz) (VirtualBox VDI, 135.79 MiB)
  - [serenity-x86_64-20221016-0ea4d22.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221016-0ea4d22.img.gz) (Raw image, 136.14 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221016-0ea4d22.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221016-0ea4d22.vdi.gz) (VirtualBox VDI, 135.79 MiB)
- [serenity-x86_64-20221016-0ea4d22.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221016-0ea4d22.img.gz) (Raw image, 136.14 MiB)

### Last commit :star:

```git
commit 0ea4d228e6e49e78ddeef623a2799c772e820dd1
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Sun Oct 16 18:04:46 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Oct 16 18:04:46 2022 +0200

    LibWeb: Add missing body has_value() check in XMLHttpRequest::send()
```
