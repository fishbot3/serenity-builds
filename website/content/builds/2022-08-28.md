---
title: 'SerenityOS build: Sunday, August 28'
date: 2022-08-28T03:06:48Z
author: Buildbot
description: |
  Commit: ba5bcb67a5 (LibWeb: Implement the HostEnsureCanAddPrivateElement JS hook, 2022-08-27)

  - [serenity-i686-20220828-ba5bcb6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220828-ba5bcb6.vdi.gz) (VirtualBox VDI, 129.71 MiB)
  - [serenity-i686-20220828-ba5bcb6.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220828-ba5bcb6.img.gz) (Raw image, 129.9 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220828-ba5bcb6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220828-ba5bcb6.vdi.gz) (VirtualBox VDI, 129.71 MiB)
- [serenity-i686-20220828-ba5bcb6.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220828-ba5bcb6.img.gz) (Raw image, 129.9 MiB)

### Last commit :star:

```git
commit ba5bcb67a56389c8949e7691eb112527ee52946f
Author:     davidot <davidot@serenityos.org>
AuthorDate: Sat Aug 27 14:35:54 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Aug 27 20:33:27 2022 +0100

    LibWeb: Implement the HostEnsureCanAddPrivateElement JS hook
    
    Also added a local test for ensuring this behavior since it is unique to
    browsers. Since we don't actually use WindowProxy anywhere yet we just
    test on location for now.
```
