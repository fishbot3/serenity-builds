---
title: 'SerenityOS build: Sunday, December 25'
date: 2022-12-25T03:58:57Z
author: Buildbot
description: |
  Commit: 2334b4cebd (Meta: Move UCD/CLDR/TZDB downloaded artifacts to Build/caches, 2022-11-25)

  - [serenity-x86_64-20221225-2334b4c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221225-2334b4c.img.gz) (Raw image, 145.05 MiB)
  - [serenity-x86_64-20221225-2334b4c.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221225-2334b4c.vdi.gz) (VirtualBox VDI, 144.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221225-2334b4c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221225-2334b4c.img.gz) (Raw image, 145.05 MiB)
- [serenity-x86_64-20221225-2334b4c.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221225-2334b4c.vdi.gz) (VirtualBox VDI, 144.73 MiB)

### Last commit :star:

```git
commit 2334b4cebdcce1104d85afa75315c2721cc2df83
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Nov 25 21:03:42 2022 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Dec 24 09:46:28 2022 -0500

    Meta: Move UCD/CLDR/TZDB downloaded artifacts to Build/caches
    
    They currently reside under Build/<arch>, meaning that they would be
    redownloaded for each architecture/toolchain build combo. Move them to a
    location that can be re-used for all builds.
```
