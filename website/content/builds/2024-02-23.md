---
title: 'SerenityOS build: Friday, February 23'
date: 2024-02-23T06:13:57Z
author: Buildbot
description: |
  Commit: 83f1775f15 (LibGfx/CCITT: Reimplement PassMode in a less naive way, 2024-02-16)

  - [serenity-x86_64-20240223-83f1775.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240223-83f1775.img.gz) (Raw image, 216.9 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240223-83f1775.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240223-83f1775.img.gz) (Raw image, 216.9 MiB)

### Last commit :star:

```git
commit 83f1775f15608f3be1b51a38c8937dc0a856c3e7
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Fri Feb 16 17:02:47 2024 -0500
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Feb 22 16:45:03 2024 +0100

    LibGfx/CCITT: Reimplement PassMode in a less naive way
    
    The old implementation of PassMode has only been tested with a single
    image, and let's say that it didn't survive long in the wild. A few
    cases were not considered:
     - We only supported VerticalMode right after PassMode.
     - It can happen that token need to be used but not consumed from the
     reference line.
    
     With that fix, we are able to decode every single PDF file from the
     1000-file zip "0000" (except 0000871.pdf, which uses byte alignment).
     This is massive progress compared to the hundred of errors that we were
     previously receiving.
```
