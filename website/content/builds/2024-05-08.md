---
title: 'SerenityOS build: Wednesday, May 08'
date: 2024-05-08T05:17:59Z
author: Buildbot
description: |
  Commit: 3f113e728f (LibWeb/SVG: Stub SVGTransform.setSkewY, 2024-05-06)

  - [serenity-x86_64-20240508-3f113e7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240508-3f113e7.img.gz) (Raw image, 219.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240508-3f113e7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240508-3f113e7.img.gz) (Raw image, 219.62 MiB)

### Last commit :star:

```git
commit 3f113e728f58dbec760de0e1170bfdeaf971c912
Author:     Jamie Mansfield <jmansfield@cadixdev.org>
AuthorDate: Mon May 6 12:25:13 2024 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Tue May 7 17:33:27 2024 -0600

    LibWeb/SVG: Stub SVGTransform.setSkewY
```
