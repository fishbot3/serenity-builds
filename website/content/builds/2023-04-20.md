---
title: 'SerenityOS build: Thursday, April 20'
date: 2023-04-20T05:03:17Z
author: Buildbot
description: |
  Commit: d2f9645cc0 (LibWeb: Properly stop, rather than terminate, ongoing media fetches, 2023-04-19)

  - [serenity-x86_64-20230420-d2f9645.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230420-d2f9645.img.gz) (Raw image, 179.02 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230420-d2f9645.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230420-d2f9645.img.gz) (Raw image, 179.02 MiB)

### Last commit :star:

```git
commit d2f9645cc00be122625298a7d7ddbb36bb5ba3aa
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Wed Apr 19 09:20:48 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Apr 20 06:19:41 2023 +0200

    LibWeb: Properly stop, rather than terminate, ongoing media fetches
    
    We are currently using the fetch controller's terminate() method to stop
    ongoing fetches when the HTMLMediaElement load algorithm is invoked.
    This method ultimately causes the fetch response to be a network error,
    which we propagate through the HTMLMediaElement's error event. This can
    cause websites, such as Steam, to avoid attempting to play any video.
    
    The spec does not actually specify what it means to "stop" or "cancel" a
    fetching process. But we should not use terminate() as that is a defined
    spec method, and the spec does tend to indicate when that method should
    be used (e.g. as it does in XMLHttpRequest).
```
