---
title: 'SerenityOS build: Wednesday, April 19'
date: 2023-04-19T05:03:33Z
author: Buildbot
description: |
  Commit: 0e8ef1b886 (LibGfx: Prevent out-of-bounds accumulation in PathRasterizer, 2023-04-18)

  - [serenity-x86_64-20230419-0e8ef1b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230419-0e8ef1b.img.gz) (Raw image, 178.95 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230419-0e8ef1b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230419-0e8ef1b.img.gz) (Raw image, 178.95 MiB)

### Last commit :star:

```git
commit 0e8ef1b8862a7acd3edfedb9b19f1ca2cb837220
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Tue Apr 18 23:55:57 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Apr 19 06:05:10 2023 +0200

    LibGfx: Prevent out-of-bounds accumulation in PathRasterizer
    
    Negative accumulation on the right-hand side of the accumulation bitmap
    would wrap around to the left, causing a negative start for certain
    lines which resulted in horizontal streaks of lower alpha values.
    Prevent this by not writing out of bounds :^)
    
    Fixes #18394
```
