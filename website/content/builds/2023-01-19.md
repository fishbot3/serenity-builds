---
title: 'SerenityOS build: Thursday, January 19'
date: 2023-01-19T06:09:07Z
author: Buildbot
description: |
  Commit: afc055c088 (LibWeb: Convert the Location object to IDL, 2023-01-18)

  - [serenity-x86_64-20230119-afc055c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230119-afc055c.img.gz) (Raw image, 680.42 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230119-afc055c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230119-afc055c.img.gz) (Raw image, 680.42 MiB)

### Last commit :star:

```git
commit afc055c088d800b505e1dbdb68f7a0f1b5a6af59
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Wed Jan 18 17:41:12 2023 +0000
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Wed Jan 18 17:36:39 2023 -0500

    LibWeb: Convert the Location object to IDL
    
    This includes:
    
    - Moving it from Bindings/ to HTML/
    - Renaming it from LocationObject to Location
    - Removing the manual definitions of the constructor and prototype
    - Removing special handling of the Location interface from the bindings
      generator
    - Converting the JS_DEFINE_NATIVE_FUNCTIONs to regular functions
      returning DeprecatedString instead of PrimitiveString
    - Adding missing (no-op) setters for the various attributes, which are
      expected to exist by the bindings generator
```
