---
title: 'SerenityOS build: Monday, January 16'
date: 2023-01-16T06:09:23Z
author: Buildbot
description: |
  Commit: 3ed9627f4e (LibIPC: Decode messages using Core::Stream internally, 2023-01-14)

  - [serenity-x86_64-20230116-3ed9627.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230116-3ed9627.img.gz) (Raw image, 680.83 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230116-3ed9627.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230116-3ed9627.img.gz) (Raw image, 680.83 MiB)

### Last commit :star:

```git
commit 3ed9627f4e85cd632e7e95b681e3733bed9a2046
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Sat Jan 14 22:04:55 2023 +0100
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Jan 15 23:06:31 2023 -0500

    LibIPC: Decode messages using Core::Stream internally
```
