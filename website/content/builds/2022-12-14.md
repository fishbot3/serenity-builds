---
title: 'SerenityOS build: Wednesday, December 14'
date: 2022-12-14T08:40:18Z
author: Buildbot
description: |
  Commit: f050534991 (Lagom: Don't compile with `-fPIC` on WIN32, 2022-12-12)

  - [serenity-x86_64-20221214-f050534.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221214-f050534.vdi.gz) (VirtualBox VDI, 143.42 MiB)
  - [serenity-x86_64-20221214-f050534.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221214-f050534.img.gz) (Raw image, 143.71 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221214-f050534.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221214-f050534.vdi.gz) (VirtualBox VDI, 143.42 MiB)
- [serenity-x86_64-20221214-f050534.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221214-f050534.img.gz) (Raw image, 143.71 MiB)

### Last commit :star:

```git
commit f0505349914af1b0b32e441e2ef0e3618006b455
Author:     Filiph Siitam Sandström <filiph.sandstrom@filfatstudios.com>
AuthorDate: Mon Dec 12 22:57:36 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Dec 13 21:53:29 2022 +0000

    Lagom: Don't compile with `-fPIC` on WIN32
```
