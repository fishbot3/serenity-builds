---
title: 'SerenityOS build: Saturday, September 16'
date: 2023-09-16T05:09:54Z
author: Buildbot
description: |
  Commit: e7a5a2e146 (Base: Update serenity-application template so that it compiles, 2023-09-15)

  - [serenity-x86_64-20230916-e7a5a2e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230916-e7a5a2e.img.gz) (Raw image, 186.18 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230916-e7a5a2e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230916-e7a5a2e.img.gz) (Raw image, 186.18 MiB)

### Last commit :star:

```git
commit e7a5a2e146a2e7dafa48f4535317bd39c76119bc
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Fri Sep 15 18:05:42 2023 +0100
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Sep 15 17:26:17 2023 -0400

    Base: Update serenity-application template so that it compiles
```
