---
title: 'SerenityOS build: Thursday, May 26'
date: 2022-05-26T03:06:19Z
author: Buildbot
description: |
  Commit: a54b681149 (LibGUI: Allow to lowercase conversion in Vim emulation, 2022-05-18)

  - [serenity-i686-20220526-a54b681.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220526-a54b681.vdi.gz) (VirtualBox VDI, 122.76 MiB)
  - [serenity-i686-20220526-a54b681.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220526-a54b681.img.gz) (Raw image, 122.95 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220526-a54b681.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220526-a54b681.vdi.gz) (VirtualBox VDI, 122.76 MiB)
- [serenity-i686-20220526-a54b681.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220526-a54b681.img.gz) (Raw image, 122.95 MiB)

### Last commit :star:

```git
commit a54b68114940d070e309c0b6c0e0691fec4c4cb3
Author:     huttongrabiel <huttonthomas@icloud.com>
AuthorDate: Wed May 18 12:09:55 2022 -0700
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu May 26 00:33:01 2022 +0100

    LibGUI: Allow to lowercase conversion in Vim emulation
    
    If Key_U is pressed while in visual mode, the currently selected text
    will be converted to lowercase.
```
