---
title: 'SerenityOS build: Tuesday, May 23'
date: 2023-05-23T05:03:49Z
author: Buildbot
description: |
  Commit: 80d6330a26 (LibWeb: Don't create mutation record node lists if nobody is interested, 2023-05-17)

  - [serenity-x86_64-20230523-80d6330.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230523-80d6330.img.gz) (Raw image, 182.83 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230523-80d6330.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230523-80d6330.img.gz) (Raw image, 182.83 MiB)

### Last commit :star:

```git
commit 80d6330a26f8c55a1bd38fd2f1388e50c0ae8977
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Wed May 17 19:47:16 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue May 23 06:31:37 2023 +0200

    LibWeb: Don't create mutation record node lists if nobody is interested
    
    By deferring allocation of StaticNodeList objects until we know somebody
    actually wants the MutationRecord, we avoid a *lot* of allocation work.
    
    This shaves several seconds off of loading https://tc39.es/ecma262/
    
    At least one other engine (WebKit) skips creating mutation records if
    nobody is interested, so even if this is observable somehow, we would
    at least match the behavior of a major engine.
```
