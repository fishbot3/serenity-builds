---
title: 'SerenityOS build: Tuesday, December 12'
date: 2023-12-12T06:10:53Z
author: Buildbot
description: |
  Commit: 5204020d42 (LibWeb: Add missing DOM::Position::visit_edges(), 2023-12-12)

  - [serenity-x86_64-20231212-5204020.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231212-5204020.img.gz) (Raw image, 206.29 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231212-5204020.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231212-5204020.img.gz) (Raw image, 206.29 MiB)

### Last commit :star:

```git
commit 5204020d4218577a21b938c0ede5a23ee22d6a94
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Tue Dec 12 01:08:07 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Dec 12 01:08:07 2023 +0100

    LibWeb: Add missing DOM::Position::visit_edges()
    
    Caught by running the test suite under ASAN.
```
