---
title: 'SerenityOS build: Sunday, August 06'
date: 2023-08-06T05:07:15Z
author: Buildbot
description: |
  Commit: f24aab662f (Ports: Update freeciv to version 3.0.8, 2023-08-01)

  - [serenity-x86_64-20230806-f24aab6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230806-f24aab6.img.gz) (Raw image, 189.2 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230806-f24aab6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230806-f24aab6.img.gz) (Raw image, 189.2 MiB)

### Last commit :star:

```git
commit f24aab662f9723c9fda80f50b6a55333eecdb996
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Tue Aug 1 20:23:37 2023 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sat Aug 5 22:29:57 2023 +0200

    Ports: Update freeciv to version 3.0.8
    
    This commit also removes a logging patch, as these changes have now
    been upstreamed.
```
