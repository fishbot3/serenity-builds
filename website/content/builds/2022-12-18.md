---
title: 'SerenityOS build: Sunday, December 18'
date: 2022-12-18T03:58:24Z
author: Buildbot
description: |
  Commit: 74927ac76d (Meta: Fix `mke2fs` on MacOS, 2022-12-15)

  - [serenity-x86_64-20221218-74927ac.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221218-74927ac.img.gz) (Raw image, 144.62 MiB)
  - [serenity-x86_64-20221218-74927ac.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221218-74927ac.vdi.gz) (VirtualBox VDI, 144.31 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221218-74927ac.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221218-74927ac.img.gz) (Raw image, 144.62 MiB)
- [serenity-x86_64-20221218-74927ac.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221218-74927ac.vdi.gz) (VirtualBox VDI, 144.31 MiB)

### Last commit :star:

```git
commit 74927ac76d464ea81008e99f57e4eb6ebc197742
Author:     EWouters <6179932+EWouters@users.noreply.github.com>
AuthorDate: Thu Dec 15 16:25:44 2022 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Dec 17 16:12:48 2022 -0700

    Meta: Fix `mke2fs` on MacOS
    
    Use `Meta/.shell_include.sh` to find the `mke2fs` executable.
```
