---
title: 'SerenityOS build: Sunday, September 04'
date: 2022-09-04T03:06:55Z
author: Buildbot
description: |
  Commit: 6dd7a536c6 (Base: Improve 1 emoji, 2022-09-03)

  - [serenity-i686-20220904-6dd7a53.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220904-6dd7a53.vdi.gz) (VirtualBox VDI, 129.8 MiB)
  - [serenity-i686-20220904-6dd7a53.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220904-6dd7a53.img.gz) (Raw image, 129.97 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220904-6dd7a53.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220904-6dd7a53.vdi.gz) (VirtualBox VDI, 129.8 MiB)
- [serenity-i686-20220904-6dd7a53.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220904-6dd7a53.img.gz) (Raw image, 129.97 MiB)

### Last commit :star:

```git
commit 6dd7a536c6f748731a8be08d788be82b0331cf22
Author:     electrikmilk <brandonjordan124@gmail.com>
AuthorDate: Sat Sep 3 14:24:48 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Sep 3 21:10:05 2022 +0100

    Base: Improve 1 emoji
    
    ♋ - U+264B CANCER
```
