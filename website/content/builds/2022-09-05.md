---
title: 'SerenityOS build: Monday, September 05'
date: 2022-09-05T03:07:29Z
author: Buildbot
description: |
  Commit: 4abb4317aa (Meta: Add frhun to the contributors list :^), 2022-09-04)

  - [serenity-i686-20220905-4abb431.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220905-4abb431.img.gz) (Raw image, 130 MiB)
  - [serenity-i686-20220905-4abb431.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220905-4abb431.vdi.gz) (VirtualBox VDI, 129.83 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220905-4abb431.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220905-4abb431.img.gz) (Raw image, 130 MiB)
- [serenity-i686-20220905-4abb431.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220905-4abb431.vdi.gz) (VirtualBox VDI, 129.83 MiB)

### Last commit :star:

```git
commit 4abb4317aa944d0eb61c1db74ed046bdafec9119
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Sun Sep 4 20:05:04 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Sep 4 20:05:04 2022 +0100

    Meta: Add frhun to the contributors list :^)
```
