---
title: 'SerenityOS build: Saturday, October 21'
date: 2023-10-21T05:08:57Z
author: Buildbot
description: |
  Commit: b7b57523cc (Ports/ClassiCube: Update ClassiCube to version 1.3.6, 2023-10-20)

  - [serenity-x86_64-20231021-b7b5752.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231021-b7b5752.img.gz) (Raw image, 190.14 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231021-b7b5752.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231021-b7b5752.img.gz) (Raw image, 190.14 MiB)

### Last commit :star:

```git
commit b7b57523cc9abc307aac2a4c49470c886f3ccd6f
Author:     cflip <cflip@cflip.net>
AuthorDate: Fri Oct 20 11:35:46 2023 -0600
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Fri Oct 20 23:24:03 2023 +0200

    Ports/ClassiCube: Update ClassiCube to version 1.3.6
```
