---
title: 'SerenityOS build: Sunday, June 11'
date: 2023-06-11T13:21:11Z
author: Buildbot
description: |
  Commit: 8bd68198d6 (Userland: Filter out unsupported file types in open dialogs in more apps, 2023-05-28)

  - [serenity-x86_64-20230611-8bd6819.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230611-8bd6819.img.gz) (Raw image, 184 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230611-8bd6819.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230611-8bd6819.img.gz) (Raw image, 184 MiB)

### Last commit :star:

```git
commit 8bd68198d6f070163549c46bfc16076e43e24202
Author:     Karol Kosek <krkk@serenityos.org>
AuthorDate: Sun May 28 17:57:02 2023 +0200
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Sun Jun 11 09:40:17 2023 +0100

    Userland: Filter out unsupported file types in open dialogs in more apps
```
