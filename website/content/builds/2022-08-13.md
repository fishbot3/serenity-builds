---
title: 'SerenityOS build: Saturday, August 13'
date: 2022-08-13T03:05:40Z
author: Buildbot
description: |
  Commit: 2e0ed98bc5 (Base: Add seven new emoji, 2022-08-11)

  - [serenity-i686-20220813-2e0ed98.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220813-2e0ed98.img.gz) (Raw image, 127.85 MiB)
  - [serenity-i686-20220813-2e0ed98.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220813-2e0ed98.vdi.gz) (VirtualBox VDI, 127.68 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220813-2e0ed98.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220813-2e0ed98.img.gz) (Raw image, 127.85 MiB)
- [serenity-i686-20220813-2e0ed98.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220813-2e0ed98.vdi.gz) (VirtualBox VDI, 127.68 MiB)

### Last commit :star:

```git
commit 2e0ed98bc5284e7b4f5e031b0a9372d6f09a6e78
Author:     Beckett Normington <beckett@b0ba.dev>
AuthorDate: Thu Aug 11 21:42:48 2022 -0400
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Fri Aug 12 12:48:07 2022 +0100

    Base: Add seven new emoji
    
    This commit adds seven more emojis to the system.
    
    🌈 - U+1F308 RAINBOW
    🌲 - U+1F332 EVERGREEN TREE
    🔺 - U+1F53A UP-POINTING RED TRIANGLE
    🔻 - U+1F53B DOWN-POINTING RED TRIANGLE
    🚥 - U+1F6A5 HORIZONTAL TRAFFIC LIGHT
    🚦 - U+1F6A6 VERTICAL TRAFFIC LIGHT
    🇨🇮 - U+1F1E8 U+1F1EE CI Côte d'Ivoire
```
