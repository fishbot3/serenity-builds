---
title: 'SerenityOS build: Sunday, September 24'
date: 2023-09-24T05:10:18Z
author: Buildbot
description: |
  Commit: f9520af71e (LibWeb: Do page_did_start_loading if history state is pushed/replaced, 2023-09-23)

  - [serenity-x86_64-20230924-f9520af.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230924-f9520af.img.gz) (Raw image, 186.29 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230924-f9520af.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230924-f9520af.img.gz) (Raw image, 186.29 MiB)

### Last commit :star:

```git
commit f9520af71ead39f02995fbb51852601eaa5c6a48
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sat Sep 23 23:08:32 2023 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Sep 23 17:59:10 2023 -0600

    LibWeb: Do page_did_start_loading if history state is pushed/replaced
    
    page_did_start_loading need to be called when current history entry
    changes to update URL in UI.
```
