---
title: 'SerenityOS build: Monday, September 26'
date: 2022-09-26T03:56:20Z
author: Buildbot
description: |
  Commit: f6264523b9 (LibWeb: Fix destination bitmap edge clip case in transform painting, 2022-09-26)

  - [serenity-i686-20220926-f626452.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220926-f626452.vdi.gz) (VirtualBox VDI, 128.88 MiB)
  - [serenity-i686-20220926-f626452.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220926-f626452.img.gz) (Raw image, 129.07 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220926-f626452.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220926-f626452.vdi.gz) (VirtualBox VDI, 128.88 MiB)
- [serenity-i686-20220926-f626452.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220926-f626452.img.gz) (Raw image, 129.07 MiB)

### Last commit :star:

```git
commit f6264523b9ef1b82ff3e7869c21bc0c819403aef
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Mon Sep 26 00:01:31 2022 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Sep 26 01:38:30 2022 +0200

    LibWeb: Fix destination bitmap edge clip case in transform painting
    
    This fixes an edge case, where the destination rect falls partly
    outside the painter, so is clipped to a smaller size in
    `get_region_bitmap()` (which needs to be accounted for with an extra
    offset).
```
