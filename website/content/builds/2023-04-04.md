---
title: 'SerenityOS build: Tuesday, April 04'
date: 2023-04-04T05:02:56Z
author: Buildbot
description: |
  Commit: bd2011406e (Kernel: Merge x86_64 and aarch64 init.cpp files, 2023-04-01)

  - [serenity-x86_64-20230404-bd20114.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230404-bd20114.img.gz) (Raw image, 175.39 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230404-bd20114.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230404-bd20114.img.gz) (Raw image, 175.39 MiB)

### Last commit :star:

```git
commit bd2011406ed9c2c7aa3964c7fad563db7abc059c
Author:     Timon Kruiper <timonkruiper@gmail.com>
AuthorDate: Sat Apr 1 02:10:19 2023 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Mon Apr 3 20:01:28 2023 -0600

    Kernel: Merge x86_64 and aarch64 init.cpp files
```
