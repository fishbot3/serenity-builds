---
title: 'SerenityOS build: Tuesday, November 22'
date: 2022-11-22T04:00:22Z
author: Buildbot
description: |
  Commit: 0c577b4b3b (Base: Add test for multiple line names in CSS Grid, 2022-11-21)

  - [serenity-x86_64-20221122-0c577b4.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221122-0c577b4.vdi.gz) (VirtualBox VDI, 140.27 MiB)
  - [serenity-x86_64-20221122-0c577b4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221122-0c577b4.img.gz) (Raw image, 140.57 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221122-0c577b4.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221122-0c577b4.vdi.gz) (VirtualBox VDI, 140.27 MiB)
- [serenity-x86_64-20221122-0c577b4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221122-0c577b4.img.gz) (Raw image, 140.57 MiB)

### Last commit :star:

```git
commit 0c577b4b3ba1943aa25778cf26a31bc66aad94ff
Author:     martinfalisse <martinmotteditfalisse@gmail.com>
AuthorDate: Mon Nov 21 19:47:31 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Nov 21 21:48:25 2022 +0000

    Base: Add test for multiple line names in CSS Grid
```
