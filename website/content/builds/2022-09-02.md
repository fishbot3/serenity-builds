---
title: 'SerenityOS build: Friday, September 02'
date: 2022-09-02T03:10:22Z
author: Buildbot
description: |
  Commit: 72a812a0c3 (Base: Add more emoji, 2022-09-02)

  - [serenity-i686-20220902-72a812a.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220902-72a812a.img.gz) (Raw image, 129.98 MiB)
  - [serenity-i686-20220902-72a812a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220902-72a812a.vdi.gz) (VirtualBox VDI, 129.8 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220902-72a812a.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220902-72a812a.img.gz) (Raw image, 129.98 MiB)
- [serenity-i686-20220902-72a812a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220902-72a812a.vdi.gz) (VirtualBox VDI, 129.8 MiB)

### Last commit :star:

```git
commit 72a812a0c32b3a2fb13ae08d7b63b375319ce402
Author:     Xexxa <93391300+Xexxa@users.noreply.github.com>
AuthorDate: Fri Sep 2 02:19:11 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Sep 2 02:11:58 2022 +0100

    Base: Add more emoji
    
    💫 - U+1F4AB DIZZY
    💨 - U+1F4A8 DASHING AWAY
    👤 - U+1F464 BUST IN SILHOUETTE
    👥 - U+1F465 BUSTS IN SILHOUETTE
    🦇 - U+1F987 BAT
    🦈 - U+1F988 SHARK
    🐝 - U+1F41D HONEYBEE
    🥀 - U+1F940 WILTED FLOWER
    🦀 - U+1F980 CRAB
    🦑 - U+1F991 SQUID
    🥂 - U+1F942 CLINKING GLASSES
    🚒 - U+1F692 FIRE ENGINE
    🚔 - U+1F694 ONCOMING POLICE CAR
    ⛵ - U+26F5 SAILBOAT
    🛎️ - U+1F6CE BELLHOP BELL
    ⌚ - U+231A WATCH
    🌠 - U+1F320 SHOOTING STAR
    🥉 - U+1F949 3RD PLACE MEDAL
    🥊 - U+1F94A BOXING GLOVE
    👔 - U+1F454 NECKTIE
    🧦 - U+1F9E6 SOCKS
    💄 - U+1F484 LIPSTICK
    🥁 - U+1F941 DRUM
    📠 - U+1F4E0 FAX MACHINE
    ❓ - U+2753 RED QUESTION MARK
    ❔ - U+2754 WHITE QUESTION MARK
    ❕ - U+2755 WHITE EXCLAMATION MARK
    ❗ - U+2757 RED EXCLAMATION MARK
```
