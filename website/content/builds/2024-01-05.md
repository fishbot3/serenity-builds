---
title: 'SerenityOS build: Friday, January 05'
date: 2024-01-05T06:11:15Z
author: Buildbot
description: |
  Commit: a1d669fe63 (RequestServer: Make pre-connection job refcounted, 2024-01-04)

  - [serenity-x86_64-20240105-a1d669f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240105-a1d669f.img.gz) (Raw image, 206.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240105-a1d669f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240105-a1d669f.img.gz) (Raw image, 206.81 MiB)

### Last commit :star:

```git
commit a1d669fe6394cb1bbf8708008baf8b6b8376cf24
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Thu Jan 4 16:40:20 2024 +0000
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Fri Jan 5 04:03:32 2024 +0330

    RequestServer: Make pre-connection job refcounted
    
    Fixes #22582
    
    Previously, the job and the (cache of them) would lead to a UAF, as
    after `.start()` was called on the job it'd be immediately destroyed.
    
    Example of previous bug:
    
    ```
    // Note due to the cache &jobA == &jobB
    auto& jobA = Job::ensure("https://r.bing.com/");
    auto& jobB = Job::ensure("https://r.bing.com/");
    // Previously, the first .start() free'd the job
    jobA.start();
    // So the second .start() was a UAF
    jobB.start();
    ```
```
