---
title: 'SerenityOS build: Monday, November 27'
date: 2023-11-27T06:10:25Z
author: Buildbot
description: |
  Commit: f34da6396f (LibPDF: Update font size after getting font from cache, 2023-11-27)

  - [serenity-x86_64-20231127-f34da63.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231127-f34da63.img.gz) (Raw image, 205.02 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231127-f34da63.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231127-f34da63.img.gz) (Raw image, 205.02 MiB)

### Last commit :star:

```git
commit f34da6396fcec8c9faa222218feb78bb57cad624
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Nov 27 08:07:45 2023 +0900
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Nov 26 19:05:13 2023 -0500

    LibPDF: Update font size after getting font from cache
    
    Page 1 of 0000277.pdf does:
    
        BT 22 0 0 22  59  28 Tm /TT2 1 Tf
            (Presented at Photonics West OPTO, February 17, 2016) Tj ET
        BT 32 0 0 32 269 426 Tm /TT1 1 Tf
            (Robert W. Boyd) Tj ET
        BT 22 0 0 22 253 357 Tm /TT2 1 Tf
            (Department of Physics and) Tj ET
        BT 22 0 0 22 105 326 Tm /TT2 1 Tf
            (Max-Planck Centre for Extreme and Quantum Photonics) Tj ET
    
    Every line begins a text operation, then updates the font matrix,
    selects a font (TT2, TT1, TT2, TT1), draws some text and ends the text
    operation.
    
    `Tm` (which sets the font matrix) contains a scale, and uses that
    to update the font size of the currently-active font (cf #20084).
    But in this file, we `Tm` first and `Tf` (font selection) second,
    so this updates the size of the old font. So when we pull it out
    of the cache again on line 3, it would still have the old size
    from the `Tm` on line 2.
    
    (The whole text scaling logic in LibPDF imho needs a rethink; the
    current approach also causes issues with zero-width glyphs which
    currently lead to divisions by zero. But that's for another PR.)
    
    Fixes another regression from c8510b58a366320 (which I've accidentally
    referred to by 2340e834cd in another commit).
```
