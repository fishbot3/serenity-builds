---
title: 'SerenityOS build: Monday, October 03'
date: 2022-10-03T03:56:35Z
author: Buildbot
description: |
  Commit: 9f7a68136f (LibWeb: It's AK_OS_MACOS, not AK_OS_MAC, 2022-10-03)

  - [serenity-i686-20221003-9f7a681.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221003-9f7a681.img.gz) (Raw image, 130.42 MiB)
  - [serenity-i686-20221003-9f7a681.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221003-9f7a681.vdi.gz) (VirtualBox VDI, 130.24 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20221003-9f7a681.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221003-9f7a681.img.gz) (Raw image, 130.42 MiB)
- [serenity-i686-20221003-9f7a681.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221003-9f7a681.vdi.gz) (VirtualBox VDI, 130.24 MiB)

### Last commit :star:

```git
commit 9f7a68136f8d0bbe46b51c2838ae5f6170058724
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Mon Oct 3 00:26:58 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Oct 3 00:27:21 2022 +0200

    LibWeb: It's AK_OS_MACOS, not AK_OS_MAC
```
