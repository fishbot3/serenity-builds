---
title: 'SerenityOS build: Thursday, November 16'
date: 2023-11-16T06:09:54Z
author: Buildbot
description: |
  Commit: 4164af2ca4 (LibWeb: Do not compensate padding for abspos boxes with static position, 2023-11-15)

  - [serenity-x86_64-20231116-4164af2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231116-4164af2.img.gz) (Raw image, 203.19 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231116-4164af2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231116-4164af2.img.gz) (Raw image, 203.19 MiB)

### Last commit :star:

```git
commit 4164af2ca4a94b50f69991c0463a9d96d83653b6
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Wed Nov 15 13:32:16 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Nov 15 23:44:05 2023 +0100

    LibWeb: Do not compensate padding for abspos boxes with static position
    
    When a box does not have a top, left, bottom, or right, there is no
    need to adjust the offset for positioning relative to the padding edge,
    because the box remains in the normal flow.
```
