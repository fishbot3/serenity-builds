---
title: 'SerenityOS build: Tuesday, March 07'
date: 2023-03-07T06:02:02Z
author: Buildbot
description: |
  Commit: 2258fc273c (LibWeb/HighResolutionTime: Add IDL typedef for DOMHighResTimeStamp, 2023-03-06)

  - [serenity-x86_64-20230307-2258fc2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230307-2258fc2.img.gz) (Raw image, 166.55 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230307-2258fc2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230307-2258fc2.img.gz) (Raw image, 166.55 MiB)

### Last commit :star:

```git
commit 2258fc273c9eb0db404a8020b8621fab31d1f7f7
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Mon Mar 6 23:56:20 2023 +0000
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Mar 6 23:57:25 2023 +0000

    LibWeb/HighResolutionTime: Add IDL typedef for DOMHighResTimeStamp
    
    We already have this for C++ code in DOMHighResTimeStamp.h, but let's
    also avoid using plain 'double' in IDL code.
```
