---
title: 'SerenityOS build: Tuesday, December 26'
date: 2023-12-26T06:10:55Z
author: Buildbot
description: |
  Commit: 8a4416837a (LibWebView: Request repaint after replacing backing stores, 2023-12-25)

  - [serenity-x86_64-20231226-8a44168.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231226-8a44168.img.gz) (Raw image, 206.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231226-8a44168.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231226-8a44168.img.gz) (Raw image, 206.06 MiB)

### Last commit :star:

```git
commit 8a4416837a6c2e7b11df6a7c29fbe68de443b0ca
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Mon Dec 25 22:58:03 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Dec 25 23:54:26 2023 +0100

    LibWebView: Request repaint after replacing backing stores
    
    Fixes a bug when the Qt client does not repaint after resizing
    when the following sequence of IPC calls happens:
    
    1. In the resize handler, the client sends set_viewport_rect to
       WebContent.
    2. WebContent starts repainting in response to the changed viewport
       size.
    3. In the resize handler, the client updates backing stores and sends
       new ids and shared bitmaps to WebContent using the add_backing_store
       call.
    4. WebContent sends an acknowledgment to the client that painting
       finished using the old id.
    5. The client discards the repaint because it expects a new backing
       store id.
```
