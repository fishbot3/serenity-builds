---
title: 'SerenityOS build: Tuesday, October 17'
date: 2023-10-17T05:09:14Z
author: Buildbot
description: |
  Commit: c41e742de4 (LibWeb: Fix min-content width calculation in dimension_box_on_line(), 2023-10-16)

  - [serenity-x86_64-20231017-c41e742.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231017-c41e742.img.gz) (Raw image, 190.34 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231017-c41e742.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231017-c41e742.img.gz) (Raw image, 190.34 MiB)

### Last commit :star:

```git
commit c41e742de4fc82aeedc0700b87ce5e0c009d57d4
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Mon Oct 16 19:28:29 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Oct 16 21:44:48 2023 +0200

    LibWeb: Fix min-content width calculation in dimension_box_on_line()
    
    If inline-block is sized under min-content width constraint we should
    use its min-content width instead of max-content width like it was
    before.
```
