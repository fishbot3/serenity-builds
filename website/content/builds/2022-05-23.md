---
title: 'SerenityOS build: Monday, May 23'
date: 2022-05-23T03:05:39Z
author: Buildbot
description: |
  Commit: f19aad8336 (Ports: Port GNU guile, 2022-05-18)

  - [serenity-i686-20220523-f19aad8.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220523-f19aad8.img.gz) (Raw image, 122.89 MiB)
  - [serenity-i686-20220523-f19aad8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220523-f19aad8.vdi.gz) (VirtualBox VDI, 122.69 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220523-f19aad8.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220523-f19aad8.img.gz) (Raw image, 122.89 MiB)
- [serenity-i686-20220523-f19aad8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220523-f19aad8.vdi.gz) (VirtualBox VDI, 122.69 MiB)

### Last commit :star:

```git
commit f19aad8336cf3c53a2f805be121f49fcf3a3877f
Author:     Peter Elliott <pelliott@ualberta.ca>
AuthorDate: Wed May 18 00:29:25 2022 -0600
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon May 23 00:13:26 2022 +0200

    Ports: Port GNU guile
```
