---
title: 'SerenityOS build: Saturday, September 17'
date: 2022-09-17T04:13:54Z
author: Buildbot
description: |
  Commit: f547b9be7b (PixelPaint: Scale layer and preserve aspect ratio, 2022-08-13)

  - [serenity-i686-20220917-f547b9b.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220917-f547b9b.img.gz) (Raw image, 128.14 MiB)
  - [serenity-i686-20220917-f547b9b.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220917-f547b9b.vdi.gz) (VirtualBox VDI, 127.96 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220917-f547b9b.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220917-f547b9b.img.gz) (Raw image, 128.14 MiB)
- [serenity-i686-20220917-f547b9b.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220917-f547b9b.vdi.gz) (VirtualBox VDI, 127.96 MiB)

### Last commit :star:

```git
commit f547b9be7ba7186980b3890d69ad6e9607e579f9
Author:     Tommaso Peduzzi <tommaso.peduzzi@gmx.ch>
AuthorDate: Sat Aug 13 16:44:18 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Sep 16 22:53:07 2022 +0200

    PixelPaint: Scale layer and preserve aspect ratio
    
    This patch adds the ability to scale a layer and
    preserve the aspect ratio.
    When the Shift key is pressed, the aspect ratio is preserved.
```
