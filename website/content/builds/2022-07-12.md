---
title: 'SerenityOS build: Tuesday, July 12'
date: 2022-07-12T03:05:23Z
author: Buildbot
description: |
  Commit: 31fc1990fa (LibWeb: Set up both containing block sizes before intrinsic sizing, 2022-07-12)

  - [serenity-i686-20220712-31fc199.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220712-31fc199.img.gz) (Raw image, 126.67 MiB)
  - [serenity-i686-20220712-31fc199.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220712-31fc199.vdi.gz) (VirtualBox VDI, 126.49 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220712-31fc199.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220712-31fc199.img.gz) (Raw image, 126.67 MiB)
- [serenity-i686-20220712-31fc199.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220712-31fc199.vdi.gz) (VirtualBox VDI, 126.49 MiB)

### Last commit :star:

```git
commit 31fc1990fa69612b16bd3aa59b9fefcd0c058aa9
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Tue Jul 12 02:17:17 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Jul 12 02:46:21 2022 +0200

    LibWeb: Set up both containing block sizes before intrinsic sizing
    
    When calculating e.g the min-content height of some box, we would only
    set its containing block's height to 0 in the temporary formatting
    state. The containing block width was not touched at all.
    
    This patch aims to do a bit better by resolving indefinite containing
    block sizes to INFINITY before performing intrinsic sizing. We should
    probably also find a way to resolve definite containing block sizes,
    but I'll leave that for our future selves.
```
