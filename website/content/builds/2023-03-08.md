---
title: 'SerenityOS build: Wednesday, March 08'
date: 2023-03-08T06:02:31Z
author: Buildbot
description: |
  Commit: 736f9f38ae (Kernel/Storage: Remove indication for possible future support of ATAPI, 2023-03-07)

  - [serenity-x86_64-20230308-736f9f3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230308-736f9f3.img.gz) (Raw image, 166.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230308-736f9f3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230308-736f9f3.img.gz) (Raw image, 166.62 MiB)

### Last commit :star:

```git
commit 736f9f38ae33d4a3140a4aff08c10f9d1689bfce
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Tue Mar 7 22:37:06 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Wed Mar 8 01:41:51 2023 +0100

    Kernel/Storage: Remove indication for possible future support of ATAPI
    
    There's no plan to support ATAPI in the foreseeable future. ATAPI is
    considered mostly as an extension to pass SCSI commands over ATA-link
    compatible channel (which could be a physical SATA or PATA).
    
    ATAPI is mostly used for controlling optical drives which are considered
    obsolete in 2023, and require an entire SCSI abstraction layer we don't
    exhibit with bypassing ioctls for sending specific SCSI commands in many
    control-flow sequences for actions being taken for such hardware.
    
    Therefore, let's make it clear we don't support ATAPI (SCSI over ATA)
    unless someone picks it up and proves otherwise that this can be done
    cleanly and also in a relevant way to our project.
```
