---
title: 'SerenityOS build: Sunday, November 06'
date: 2022-11-06T04:00:04Z
author: Buildbot
description: |
  Commit: 5e5cdb2d28 (LibJS: Remove the now-unused LocalTZA AO, 2022-11-05)

  - [serenity-x86_64-20221106-5e5cdb2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221106-5e5cdb2.img.gz) (Raw image, 139.35 MiB)
  - [serenity-x86_64-20221106-5e5cdb2.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221106-5e5cdb2.vdi.gz) (VirtualBox VDI, 139.03 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221106-5e5cdb2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221106-5e5cdb2.img.gz) (Raw image, 139.35 MiB)
- [serenity-x86_64-20221106-5e5cdb2.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221106-5e5cdb2.vdi.gz) (VirtualBox VDI, 139.03 MiB)

### Last commit :star:

```git
commit 5e5cdb2d286d0b2a34e1fe7b9ec1e01289170cdf
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Sat Nov 5 16:16:20 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Nov 6 01:47:37 2022 +0000

    LibJS: Remove the now-unused LocalTZA AO
```
