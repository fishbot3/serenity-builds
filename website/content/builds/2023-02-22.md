---
title: 'SerenityOS build: Wednesday, February 22'
date: 2023-02-22T06:01:34Z
author: Buildbot
description: |
  Commit: 9096b4d893 (AK: Ensure that we fill the whole String when reading from a Stream, 2023-02-21)

  - [serenity-x86_64-20230222-9096b4d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230222-9096b4d.img.gz) (Raw image, 163.65 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230222-9096b4d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230222-9096b4d.img.gz) (Raw image, 163.65 MiB)

### Last commit :star:

```git
commit 9096b4d8938b53431053cad801db8a12f9d7cb90
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Tue Feb 21 23:02:48 2023 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Tue Feb 21 22:28:15 2023 -0700

    AK: Ensure that we fill the whole String when reading from a Stream
```
