---
title: 'SerenityOS build: Thursday, September 29'
date: 2022-09-29T03:56:42Z
author: Buildbot
description: |
  Commit: 454bf1fde0 (HackStudio: Insert tooltip styling directly into Markdown HTML output, 2022-09-27)

  - [serenity-i686-20220929-454bf1f.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220929-454bf1f.img.gz) (Raw image, 129.8 MiB)
  - [serenity-i686-20220929-454bf1f.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220929-454bf1f.vdi.gz) (VirtualBox VDI, 129.64 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220929-454bf1f.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220929-454bf1f.img.gz) (Raw image, 129.8 MiB)
- [serenity-i686-20220929-454bf1f.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220929-454bf1f.vdi.gz) (VirtualBox VDI, 129.64 MiB)

### Last commit :star:

```git
commit 454bf1fde054591d1215404617126ce31b281171
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Tue Sep 27 11:40:14 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Wed Sep 28 23:58:26 2022 +0100

    HackStudio: Insert tooltip styling directly into Markdown HTML output
```
