---
title: 'SerenityOS build: Wednesday, September 20'
date: 2023-09-20T05:10:23Z
author: Buildbot
description: |
  Commit: f6c52f622d (AK: Number the spec step comments in `URL::serialize_path()`, 2023-09-19)

  - [serenity-x86_64-20230920-f6c52f6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230920-f6c52f6.img.gz) (Raw image, 186.38 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230920-f6c52f6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230920-f6c52f6.img.gz) (Raw image, 186.38 MiB)

### Last commit :star:

```git
commit f6c52f622dd42a371cfd9b8a621c642b3ee2575b
Author:     Kemal Zebari <kemalzebra@gmail.com>
AuthorDate: Tue Sep 19 09:45:12 2023 -0700
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Tue Sep 19 21:51:31 2023 +0100

    AK: Number the spec step comments in `URL::serialize_path()`
```
