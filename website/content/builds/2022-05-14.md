---
title: 'SerenityOS build: Saturday, May 14'
date: 2022-05-14T03:05:31Z
author: Buildbot
description: |
  Commit: 4472cab81a (Ports: Set right launcher command for Quake, 2022-05-13)

  - [serenity-i686-20220514-4472cab.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220514-4472cab.img.gz) (Raw image, 122.08 MiB)
  - [serenity-i686-20220514-4472cab.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220514-4472cab.vdi.gz) (VirtualBox VDI, 121.89 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220514-4472cab.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220514-4472cab.img.gz) (Raw image, 122.08 MiB)
- [serenity-i686-20220514-4472cab.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220514-4472cab.vdi.gz) (VirtualBox VDI, 121.89 MiB)

### Last commit :star:

```git
commit 4472cab81ac6f24dd1425153b05b24ef424c760a
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Fri May 13 22:45:52 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri May 13 23:01:58 2022 +0200

    Ports: Set right launcher command for Quake
    
    By setting the absolute path for `launcher_command`, the menu item
    actually shows up. Provide an `icon_file` as well so it's pretty.
```
