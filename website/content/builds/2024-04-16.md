---
title: 'SerenityOS build: Tuesday, April 16'
date: 2024-04-16T05:17:37Z
author: Buildbot
description: |
  Commit: 5d89d3090e (Kernel: Add KCOV recursion debugging, 2024-04-08)

  - [serenity-x86_64-20240416-5d89d30.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240416-5d89d30.img.gz) (Raw image, 222.09 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240416-5d89d30.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240416-5d89d30.img.gz) (Raw image, 222.09 MiB)

### Last commit :star:

```git
commit 5d89d3090e1fd22b3b5ff0e4931798e191c92bd9
Author:     Space Meyer <git@the-space.agency>
AuthorDate: Mon Apr 8 02:51:38 2024 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Mon Apr 15 21:16:22 2024 -0600

    Kernel: Add KCOV recursion debugging
```
