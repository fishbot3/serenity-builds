---
title: 'SerenityOS build: Friday, December 15'
date: 2023-12-15T06:11:27Z
author: Buildbot
description: |
  Commit: 47b6030347 (Meta: Replace run.sh by run.py, 2023-11-24)

  - [serenity-x86_64-20231215-47b6030.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231215-47b6030.img.gz) (Raw image, 206.55 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231215-47b6030.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231215-47b6030.img.gz) (Raw image, 206.55 MiB)

### Last commit :star:

```git
commit 47b6030347c9feb846cb79e9254347f8f53f8d58
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Fri Nov 24 17:28:23 2023 +0100
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Fri Dec 15 00:11:50 2023 +0100

    Meta: Replace run.sh by run.py
    
    A Python script is much easier to maintain than the organically grown
    variable mess that was run.sh.
    
    For now, the script inherits most environment variable modifiability
    from the shell script, but this is not a requirement.
    
    While porting this script, a couple of improvements have been made:
    - Spaces (especially in paths) cannot break most arguments anymore.
      Exceptions are environment variables specifying multiple arguments on
      purpose, these should be replaced in the future anyways.
    - Force control over virtualization is now possible with
      SERENITY_VIRTUALIZATION_SUPPORT. If set to 0, this variable was
      sometimes ignored before.
    - Handling Windows native QEMU is much more robust. Multiple incorrect
      checks for WSL, but not Windows native QEMU, were used before. This
      would also allow disabling native Windows QEMU much more easily in the
      future, which is necessary for GDB.
    - Various machine types had wrong or outdated arguments, such as qn.
    
    Co-Authored-By: Andrew Kaster <akaster@serenityos.org>
```
