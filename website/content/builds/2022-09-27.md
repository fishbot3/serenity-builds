---
title: 'SerenityOS build: Tuesday, September 27'
date: 2022-09-27T03:56:32Z
author: Buildbot
description: |
  Commit: 42f36d4865 (LibGUI: Make "Home" key behavior comfier on wrapped lines :^), 2022-09-26)

  - [serenity-i686-20220927-42f36d4.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220927-42f36d4.vdi.gz) (VirtualBox VDI, 128.84 MiB)
  - [serenity-i686-20220927-42f36d4.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220927-42f36d4.img.gz) (Raw image, 129 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220927-42f36d4.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220927-42f36d4.vdi.gz) (VirtualBox VDI, 128.84 MiB)
- [serenity-i686-20220927-42f36d4.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220927-42f36d4.img.gz) (Raw image, 129 MiB)

### Last commit :star:

```git
commit 42f36d4865d003bab2b19e4f378b1f30ec92cd47
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Mon Sep 26 16:52:26 2022 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Sep 26 23:12:31 2022 +0200

    LibGUI: Make "Home" key behavior comfier on wrapped lines :^)
    
    There's a big comment explaining it because it's somewhat confusing, but
    the behavior I'm copying from other editors is, when you repeatedly
    press the `Home` key:
    
    1. Go to the start of the visual line, where it wraps.
    2. Go to the first non-whitespace character on the logical line, which
       is what happens in non-wrapped lines.
    3. Go to column 0.
    4. GOTO 2
```
