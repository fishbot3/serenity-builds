---
title: 'SerenityOS build: Wednesday, August 24'
date: 2022-08-24T03:06:20Z
author: Buildbot
description: |
  Commit: a9d2d806b6 (LibJS: Use __builtin_isnan in static_assert instead of isnan, 2022-08-23)

  - [serenity-i686-20220824-a9d2d80.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220824-a9d2d80.img.gz) (Raw image, 129.73 MiB)
  - [serenity-i686-20220824-a9d2d80.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220824-a9d2d80.vdi.gz) (VirtualBox VDI, 129.56 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220824-a9d2d80.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220824-a9d2d80.img.gz) (Raw image, 129.73 MiB)
- [serenity-i686-20220824-a9d2d80.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220824-a9d2d80.vdi.gz) (VirtualBox VDI, 129.56 MiB)

### Last commit :star:

```git
commit a9d2d806b61df3b84af06744b6fad670f4012539
Author:     davidot <davidot@serenityos.org>
AuthorDate: Tue Aug 23 20:24:59 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Aug 23 22:30:15 2022 +0100

    LibJS: Use __builtin_isnan in static_assert instead of isnan
    
    For the fuzzer build isnan was not usable in a constexpr context however
    __builtin_isnan seems to always be.
    
    Also while we're here add my name to the copyright since I forgot after
    the Value rewrite.
```
