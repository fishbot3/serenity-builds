---
title: 'SerenityOS build: Wednesday, March 06'
date: 2024-03-06T06:14:58Z
author: Buildbot
description: |
  Commit: ab143e9b0e (LibGfx/BMP: Clear alpha in palette entries, 2024-03-04)

  - [serenity-x86_64-20240306-ab143e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240306-ab143e9.img.gz) (Raw image, 218.66 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240306-ab143e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240306-ab143e9.img.gz) (Raw image, 218.66 MiB)

### Last commit :star:

```git
commit ab143e9b0eba981f6154a01013ba7555bf3ee1a7
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Mar 4 19:56:13 2024 -0500
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Tue Mar 5 21:27:41 2024 +0000

    LibGfx/BMP: Clear alpha in palette entries
    
    The semantics of BGRx8888 aren't super clear and it means different
    things for different parts of the codebase. In particular, the PNG
    writer still writes the x channel to the alpha channel of its output.
    
    In BMPs, the 4th palette byte is usually 0, which means after #21412 we
    started writing all .bmp files with <= 8bpp as completely transparent
    to PNGs.
    
    This works around that.
    
    (See also #19464 for previous similar workarounds.)
    
    The added `bitmap.bmp` is a 1bpp file I drew in Photoshop and saved
    using its "Save as..." saving path.
```
