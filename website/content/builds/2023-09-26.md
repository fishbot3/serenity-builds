---
title: 'SerenityOS build: Tuesday, September 26'
date: 2023-09-26T05:09:29Z
author: Buildbot
description: |
  Commit: 6820e0e175 (LibWasm: Make sure to place imported functions before the module's, 2023-09-24)

  - [serenity-x86_64-20230926-6820e0e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230926-6820e0e.img.gz) (Raw image, 185.9 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230926-6820e0e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230926-6820e0e.img.gz) (Raw image, 185.9 MiB)

### Last commit :star:

```git
commit 6820e0e175741aa5fff91a88aa50501611fd8d96
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Sun Sep 24 21:20:45 2023 +0330
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Tue Sep 26 07:47:20 2023 +0330

    LibWasm: Make sure to place imported functions before the module's
    
    aafef1e92d8e750b8abd2100e5896df75505e2a1 broke this while trying to
    make the global import available in initialisation, this commit makes
    sure we place the module's own functions after all resolved imports.
```
