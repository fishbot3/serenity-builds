---
title: 'SerenityOS build: Thursday, February 23'
date: 2023-02-23T11:07:48Z
author: Buildbot
description: |
  Commit: b19dc8a9b6 (Kernel: Prevent out-of-bounds read/write in VirtIO GPU3DDevice::ioctl, 2023-02-22)

  - [serenity-x86_64-20230223-b19dc8a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230223-b19dc8a.img.gz) (Raw image, 163.91 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230223-b19dc8a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230223-b19dc8a.img.gz) (Raw image, 163.91 MiB)

### Last commit :star:

```git
commit b19dc8a9b61fc88ed9abfd6d0f82c308350eff59
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Wed Feb 22 20:11:03 2023 +0200
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Thu Feb 23 09:33:54 2023 +0000

    Kernel: Prevent out-of-bounds read/write in VirtIO GPU3DDevice::ioctl
    
    Before doing a check if offset_in_region + num_bytes of the transfer
    descriptor are together more than NUM_TRANSFER_REGION_PAGES * PAGE_SIZE,
    check that addition of both of these parameters will not simply overflow
    which could lead to out-of-bounds read/write.
    
    Fixes #17518.
```
