---
title: 'SerenityOS build: Thursday, July 21'
date: 2022-07-21T03:05:29Z
author: Buildbot
description: |
  Commit: 237fbe4d54 (LibWeb: Remember the used flex basis for each flex item, 2022-07-17)

  - [serenity-i686-20220721-237fbe4.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220721-237fbe4.vdi.gz) (VirtualBox VDI, 127.26 MiB)
  - [serenity-i686-20220721-237fbe4.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220721-237fbe4.img.gz) (Raw image, 127.44 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220721-237fbe4.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220721-237fbe4.vdi.gz) (VirtualBox VDI, 127.26 MiB)
- [serenity-i686-20220721-237fbe4.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220721-237fbe4.img.gz) (Raw image, 127.44 MiB)

### Last commit :star:

```git
commit 237fbe4d54b083a79b5fcf05b9b464acefa44e6f
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Sun Jul 17 19:40:02 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Jul 21 01:46:26 2022 +0200

    LibWeb: Remember the used flex basis for each flex item
    
    This will be consulted later on in the flex layout algorithm.
```
