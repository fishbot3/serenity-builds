---
title: 'SerenityOS build: Sunday, April 14'
date: 2024-04-14T05:17:23Z
author: Buildbot
description: |
  Commit: 3645b676fb (LibWeb: Remove `RecordingPainter::paint_frame()`, 2024-04-10)

  - [serenity-x86_64-20240414-3645b67.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240414-3645b67.img.gz) (Raw image, 221.71 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240414-3645b67.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240414-3645b67.img.gz) (Raw image, 221.71 MiB)

### Last commit :star:

```git
commit 3645b676fb3923d4417dcc25f9159c37b1251fcc
Author:     Arthur Grillo <arthurgrillo@riseup.net>
AuthorDate: Wed Apr 10 16:21:02 2024 -0300
Commit:     Alexander Kalenik <kalenik.aliaksandr@gmail.com>
CommitDate: Sat Apr 13 20:25:48 2024 -0700

    LibWeb: Remove `RecordingPainter::paint_frame()`
    
    PaintFrame is not primitive painting command, we inherited from OS, that
    is hard to replicate in GPU-painter or alternative CPU-painter API. We
    should remove it as a part of refactoring towards simplifying recording
    painter commands set.
    
    Fixes: #23796
```
