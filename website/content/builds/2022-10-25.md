---
title: 'SerenityOS build: Tuesday, October 25'
date: 2022-10-25T04:03:02Z
author: Buildbot
description: |
  Commit: 03ae9f94cf (Kernel/FileSystem: Remove hardcoded unveil path of /usr/lib/Loader.so, 2022-10-24)

  - [serenity-x86_64-20221025-03ae9f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221025-03ae9f9.img.gz) (Raw image, 136.9 MiB)
  - [serenity-x86_64-20221025-03ae9f9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221025-03ae9f9.vdi.gz) (VirtualBox VDI, 136.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221025-03ae9f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221025-03ae9f9.img.gz) (Raw image, 136.9 MiB)
- [serenity-x86_64-20221025-03ae9f9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221025-03ae9f9.vdi.gz) (VirtualBox VDI, 136.58 MiB)

### Last commit :star:

```git
commit 03ae9f94cfdc734d8cb036f408ab05e582e4e345
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Mon Oct 24 18:09:03 2022 +0300
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Mon Oct 24 19:41:32 2022 -0600

    Kernel/FileSystem: Remove hardcoded unveil path of /usr/lib/Loader.so
    
    If a program needs to execute a dynamic executable program, then it
    should unveil /usr/lib/Loader.so by itself and not rely on the Kernel to
    allow using this binary without any sense of respect to unveil promises
    being made by the running parent program.
```
