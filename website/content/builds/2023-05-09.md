---
title: 'SerenityOS build: Tuesday, May 09'
date: 2023-05-09T05:03:20Z
author: Buildbot
description: |
  Commit: eda2a2f5da (AK: Remove `must_set()` from `JsonArray`, 2023-05-08)

  - [serenity-x86_64-20230509-eda2a2f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230509-eda2a2f.img.gz) (Raw image, 181.53 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230509-eda2a2f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230509-eda2a2f.img.gz) (Raw image, 181.53 MiB)

### Last commit :star:

```git
commit eda2a2f5da50b593b9c0c136c27a671cfcadbcaa
Author:     Kemal Zebari <kemalzebra@gmail.com>
AuthorDate: Mon May 8 13:53:29 2023 -0700
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue May 9 06:21:34 2023 +0200

    AK: Remove `must_set()` from `JsonArray`
    
    Due to 582c55a, both `must_set()` and `set()` should be providing the
    same behavior. Not only is that a reason to remove `must_set()`, but
    it also performs erroneous behavior since it inserts an element at
    a specified index instead of modifying an element at that index.
```
