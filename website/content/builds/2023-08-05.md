---
title: 'SerenityOS build: Saturday, August 05'
date: 2023-08-05T05:07:58Z
author: Buildbot
description: |
  Commit: cd9bb985d4 (LibGfx/JPEGXL: Add support for the self-correcting predictor, 2023-07-30)

  - [serenity-x86_64-20230805-cd9bb98.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230805-cd9bb98.img.gz) (Raw image, 188.86 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230805-cd9bb98.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230805-cd9bb98.img.gz) (Raw image, 188.86 MiB)

### Last commit :star:

```git
commit cd9bb985d40b88c225e20d477f432b1f0520e280
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Sun Jul 30 23:08:54 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Aug 5 06:30:59 2023 +0200

    LibGfx/JPEGXL: Add support for the self-correcting predictor
    
    This predictor is much more complicated than the others. Indeed, to be
    computed, it needs its own value but for other pixels. As you can guess,
    implementing it involved the introduction of a structure to hold that
    data.
    
    Fundamentally, this predictor uses the value of the error between the
    predicted value and the true value (aka decoded value) of pixels around.
    One of this computed error (namely max_error) is used as a property, so
    this patch also solves a FIXME in `get_properties`.
    
    To ease the access to value that are close in the channel and moving
    their values around, this patch adds a `Neighborhood` struct which holds
    this data. It has been used in `prediction()` and it allowed to simplify
    the signature and to remove the explicit retrieval of the underlying
    data.
    
    All this work allows us to decode the default image that appears when
    loading `https://jxl-art.surma.technology`. However, we still render it
    incorrectly due to the lack of support for orientation values different
    from 1.
```
