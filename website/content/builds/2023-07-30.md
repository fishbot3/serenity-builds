---
title: 'SerenityOS build: Sunday, July 30'
date: 2023-07-30T05:07:42Z
author: Buildbot
description: |
  Commit: 0612e8ec6a (LibGfx/JPEGXL: Read data related to extra channels in `FrameHeader`, 2023-07-26)

  - [serenity-x86_64-20230730-0612e8e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230730-0612e8e.img.gz) (Raw image, 188.1 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230730-0612e8e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230730-0612e8e.img.gz) (Raw image, 188.1 MiB)

### Last commit :star:

```git
commit 0612e8ec6a8523bb6cad46f0a19307d055b6321b
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Wed Jul 26 18:06:56 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Jul 30 05:39:47 2023 +0200

    LibGfx/JPEGXL: Read data related to extra channels in `FrameHeader`
    
    Thanks to previous patches, everything used in `read_frame_header`
    supports extra channels. The last element to achieve the read of headers
    of frame with extra channels is to add support in the function itself
    and the `FrameHeader` struct, which that patch does.
```
