---
title: 'SerenityOS build: Monday, August 01'
date: 2022-08-01T03:06:00Z
author: Buildbot
description: |
  Commit: f722612e17 (Browser: Set preferred color scheme when constructing a tab, 2022-07-31)

  - [serenity-i686-20220801-f722612.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220801-f722612.img.gz) (Raw image, 128.43 MiB)
  - [serenity-i686-20220801-f722612.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220801-f722612.vdi.gz) (VirtualBox VDI, 128.27 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220801-f722612.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220801-f722612.img.gz) (Raw image, 128.43 MiB)
- [serenity-i686-20220801-f722612.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220801-f722612.vdi.gz) (VirtualBox VDI, 128.27 MiB)

### Last commit :star:

```git
commit f722612e177b6d715e814b9cdc4139abbaf4061b
Author:     networkException <git@nwex.de>
AuthorDate: Sun Jul 31 23:38:55 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Aug 1 00:20:43 2022 +0200

    Browser: Set preferred color scheme when constructing a tab
    
    Previously we would only tell OutOfProcessWebView about the preferred
    color scheme when changing it in the browser's settings.
    
    This patch implements reading in the current config value to set the
    scheme immediately :^)
    
    See SerenityOS update (July 2022) https://youtu.be/aO0b2X7tzuk?t=2171
```
