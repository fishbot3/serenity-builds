---
title: 'SerenityOS build: Wednesday, August 31'
date: 2022-08-31T03:07:17Z
author: Buildbot
description: |
  Commit: c477425b9b (LibJS: Create DurationFormat's ListFormat object with type and style, 2022-08-30)

  - [serenity-i686-20220831-c477425.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220831-c477425.vdi.gz) (VirtualBox VDI, 129.73 MiB)
  - [serenity-i686-20220831-c477425.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220831-c477425.img.gz) (Raw image, 129.92 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220831-c477425.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220831-c477425.vdi.gz) (VirtualBox VDI, 129.73 MiB)
- [serenity-i686-20220831-c477425.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220831-c477425.img.gz) (Raw image, 129.92 MiB)

### Last commit :star:

```git
commit c477425b9bd61f35efd1eb20881f0fdfcddb6cfa
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Tue Aug 30 12:04:11 2022 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Tue Aug 30 14:26:11 2022 -0400

    LibJS: Create DurationFormat's ListFormat object with type and style
    
    This is a normative change in the Intl.DurationFormat spec. See:
    https://github.com/tc39/proposal-intl-duration-format/commit/1304e4b
```
