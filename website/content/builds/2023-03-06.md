---
title: 'SerenityOS build: Monday, March 06'
date: 2023-03-06T06:02:05Z
author: Buildbot
description: |
  Commit: 918a3082d6 (LibWeb: Fix currentColor as a background-color (and maybe other places), 2023-03-05)

  - [serenity-x86_64-20230306-918a308.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230306-918a308.img.gz) (Raw image, 166.48 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230306-918a308.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230306-918a308.img.gz) (Raw image, 166.48 MiB)

### Last commit :star:

```git
commit 918a3082d6dc6c8769a67ae2fd76bed839d400cb
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sun Mar 5 22:31:44 2023 +0000
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Mar 6 00:09:13 2023 +0000

    LibWeb: Fix currentColor as a background-color (and maybe other places)
    
    This moves color to be the first value resolved, this ensures that
    calls to .to_color() on style values for other properties will always
    be able to resolve the current color.
    
    This change fixes the `background-color: currentColor` example in
    colors.html.
```
