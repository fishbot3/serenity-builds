---
title: 'SerenityOS build: Wednesday, August 17'
date: 2022-08-17T03:06:06Z
author: Buildbot
description: |
  Commit: b89cd4215e (Base: Add 10 new emojis, 2022-08-16)

  - [serenity-i686-20220817-b89cd42.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220817-b89cd42.img.gz) (Raw image, 129.23 MiB)
  - [serenity-i686-20220817-b89cd42.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220817-b89cd42.vdi.gz) (VirtualBox VDI, 129.04 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220817-b89cd42.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220817-b89cd42.img.gz) (Raw image, 129.23 MiB)
- [serenity-i686-20220817-b89cd42.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220817-b89cd42.vdi.gz) (VirtualBox VDI, 129.04 MiB)

### Last commit :star:

```git
commit b89cd4215e1635e3b203d072d18fda1356dd6899
Author:     Ryan Liptak <squeek502@hotmail.com>
AuthorDate: Tue Aug 16 14:06:39 2022 -0700
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Aug 16 23:09:49 2022 +0100

    Base: Add 10 new emojis
    
    ☹️ - U+2639 U+FE0F FROWNING FACE
    ☺️ - U+263A U+FE0F SMILING FACE
    🤡 - U+1F921 CLOWN FACE
    🥹 - U+1F979 FACE HOLDING BACK TEARS
    🫠 - U+1FAE0 MELTING FACE
    🫡 - U+1FAE1 SALUTING FACE
    🫢 - U+1FAE2 FACE WITH OPEN EYES AND HAND OVER MOUTH
    🫣 - U+1FAE3 FACE WITH PEEKING EYE
    🫤 - U+1FAE4 FACE WITH DIAGONAL MOUTH
    🫥 - U+1FAE5 DOTTED LINE FACE
```
