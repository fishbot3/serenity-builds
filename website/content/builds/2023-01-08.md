---
title: 'SerenityOS build: Sunday, January 08'
date: 2023-01-08T05:08:55Z
author: Buildbot
description: |
  Commit: a5d5b970ff (Chess: Port to `Core::Stream`, 2023-01-07)

  - [serenity-x86_64-20230108-a5d5b97.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230108-a5d5b97.img.gz) (Raw image, 669.93 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230108-a5d5b97.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230108-a5d5b97.img.gz) (Raw image, 669.93 MiB)

### Last commit :star:

```git
commit a5d5b970ff475aa52730f3b01485dec395dc404d
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Sat Jan 7 10:30:35 2023 -0500
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Sat Jan 7 20:05:24 2023 +0000

    Chess: Port to `Core::Stream`
```
