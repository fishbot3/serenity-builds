---
title: 'SerenityOS build: Tuesday, August 29'
date: 2023-08-29T05:08:09Z
author: Buildbot
description: |
  Commit: 9b884a9605 (LibJS: Avoid double construction in Array.fromAsync, 2023-08-28)

  - [serenity-x86_64-20230829-9b884a9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230829-9b884a9.img.gz) (Raw image, 185.79 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230829-9b884a9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230829-9b884a9.img.gz) (Raw image, 185.79 MiB)

### Last commit :star:

```git
commit 9b884a9605f31f9aa8f773edaa541098cea86a3e
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Mon Aug 28 20:13:01 2023 +1200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Aug 28 20:45:11 2023 -0400

    LibJS: Avoid double construction in Array.fromAsync
    
    This is a normative change in the array from async proposal, see:
    
    https://github.com/tc39/proposal-array-from-async/commit/49cfde2
    
    It fixes a double construction when Array.fromAsync is given an array
    like object.
```
