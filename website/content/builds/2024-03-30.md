---
title: 'SerenityOS build: Saturday, March 30'
date: 2024-03-30T06:16:11Z
author: Buildbot
description: |
  Commit: b1a30d8269 (LibWebView+WebContent: Remove some SPAM_DEBUG log points, 2024-03-29)

  - [serenity-x86_64-20240330-b1a30d8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240330-b1a30d8.img.gz) (Raw image, 220.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240330-b1a30d8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240330-b1a30d8.img.gz) (Raw image, 220.37 MiB)

### Last commit :star:

```git
commit b1a30d8269b7492e48075424018aee283edece8d
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Mar 29 09:46:55 2024 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Mar 29 22:00:25 2024 +0100

    LibWebView+WebContent: Remove some SPAM_DEBUG log points
    
    I don't know if anyone ever enables these anymore but I think we're well
    past needing to persist logging these particular IPC endpoints.
```
