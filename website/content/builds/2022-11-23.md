---
title: 'SerenityOS build: Wednesday, November 23'
date: 2022-11-23T04:00:25Z
author: Buildbot
description: |
  Commit: 0f1f925532 (LibJS: Shrink Identifier's environment coordinate cache, 2022-11-21)

  - [serenity-x86_64-20221123-0f1f925.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221123-0f1f925.vdi.gz) (VirtualBox VDI, 140.29 MiB)
  - [serenity-x86_64-20221123-0f1f925.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221123-0f1f925.img.gz) (Raw image, 140.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221123-0f1f925.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221123-0f1f925.vdi.gz) (VirtualBox VDI, 140.29 MiB)
- [serenity-x86_64-20221123-0f1f925.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221123-0f1f925.img.gz) (Raw image, 140.62 MiB)

### Last commit :star:

```git
commit 0f1f92553213c6c2c7268078c9d39b813c24bb49
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Mon Nov 21 18:01:22 2022 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Nov 22 21:13:35 2022 +0100

    LibJS: Shrink Identifier's environment coordinate cache
    
    This patch does two things:
    
    - We now use u32 instead of size_t for the hops and index fields
      in EnvironmentCoordinate. This means we're limited to an environment
      nesting level and variable count of 4Gs respectively.
    
    - Instead of wrapping it in an Optional, EnvironmentCoordinate now has
      a custom valid/invalid state using a magic marker value.
    
    These two changes reduce the size of Identifier by 16 bytes. :^)
```
