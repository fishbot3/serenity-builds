---
title: 'SerenityOS build: Friday, December 23'
date: 2022-12-23T03:58:43Z
author: Buildbot
description: |
  Commit: 4b4b15adb1 (AK: Rearrange Error's members to reduce its size by 8 bytes, 2022-12-22)

  - [serenity-x86_64-20221223-4b4b15a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221223-4b4b15a.vdi.gz) (VirtualBox VDI, 144.46 MiB)
  - [serenity-x86_64-20221223-4b4b15a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221223-4b4b15a.img.gz) (Raw image, 144.8 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221223-4b4b15a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221223-4b4b15a.vdi.gz) (VirtualBox VDI, 144.46 MiB)
- [serenity-x86_64-20221223-4b4b15a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221223-4b4b15a.img.gz) (Raw image, 144.8 MiB)

### Last commit :star:

```git
commit 4b4b15adb1c755bc650c999ac3c8e1b284687ad4
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Thu Dec 22 08:56:32 2022 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Thu Dec 22 14:13:56 2022 -0500

    AK: Rearrange Error's members to reduce its size by 8 bytes
    
    This shrinks sizeof(Error) from 32 bytes to 24 bytes, which in turn will
    shrink sizeof(ErrorOr<T>) by the same amount (in cases where sizeof(T)
    is less than sizeof(Error)).
```
