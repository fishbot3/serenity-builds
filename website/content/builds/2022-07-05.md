---
title: 'SerenityOS build: Tuesday, July 05'
date: 2022-07-05T03:10:35Z
author: Buildbot
description: |
  Commit: c774790975 (LibArchive: Guard against major() and minor() macros from old glibc, 2022-07-04)

  - [serenity-i686-20220705-c774790.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220705-c774790.img.gz) (Raw image, 125.41 MiB)
  - [serenity-i686-20220705-c774790.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220705-c774790.vdi.gz) (VirtualBox VDI, 125.23 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220705-c774790.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220705-c774790.img.gz) (Raw image, 125.41 MiB)
- [serenity-i686-20220705-c774790.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220705-c774790.vdi.gz) (VirtualBox VDI, 125.23 MiB)

### Last commit :star:

```git
commit c774790975f136bcff70dd4e9fa444f100f34003
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Mon Jul 4 15:33:37 2022 -0600
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Jul 5 01:27:37 2022 +0200

    LibArchive: Guard against major() and minor() macros from old glibc
    
    glibc before 2.28 defines major() and minor() macros from sys/types.h.
    
    This triggers a Lagom warning for old distros that use versions older
    than that, such as Ubuntu 18.04. This fixes a break in the
    compiler-explorer Lagom build, which is based off 18.04 docker
    containers.
```
