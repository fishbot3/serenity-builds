---
title: 'SerenityOS build: Saturday, June 11'
date: 2022-06-11T03:04:45Z
author: Buildbot
description: |
  Commit: 5caf307ec0 (WindowServer+MouseSettings: Toggle cursor highlighting with Super+H, 2022-06-07)

  - [serenity-i686-20220611-5caf307.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220611-5caf307.vdi.gz) (VirtualBox VDI, 123.69 MiB)
  - [serenity-i686-20220611-5caf307.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220611-5caf307.img.gz) (Raw image, 123.86 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220611-5caf307.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220611-5caf307.vdi.gz) (VirtualBox VDI, 123.69 MiB)
- [serenity-i686-20220611-5caf307.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220611-5caf307.img.gz) (Raw image, 123.86 MiB)

### Last commit :star:

```git
commit 5caf307ec0c23da13deb106aa7d512bedc77d73e
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Tue Jun 7 23:41:04 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Jun 10 23:02:34 2022 +0100

    WindowServer+MouseSettings: Toggle cursor highlighting with Super+H
    
    Rather than enabling/disabling cursor highlighting by going into mouse
    settings, you can now instead toggle it any time with Super+H. Mouse
    settings now is only for customising the look of the highlighting.
```
