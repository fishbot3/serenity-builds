---
title: 'SerenityOS build: Friday, May 19'
date: 2023-05-19T05:03:39Z
author: Buildbot
description: |
  Commit: 444470b238 (Applications: Improve FSAC error message handling, 2023-05-18)

  - [serenity-x86_64-20230519-444470b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230519-444470b.img.gz) (Raw image, 182.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230519-444470b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230519-444470b.img.gz) (Raw image, 182.37 MiB)

### Last commit :star:

```git
commit 444470b238eb5fa4905d1fa5383931ef84bf6a41
Author:     thankyouverycool <66646555+thankyouverycool@users.noreply.github.com>
AuthorDate: Thu May 18 08:50:38 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri May 19 06:20:41 2023 +0200

    Applications: Improve FSAC error message handling
    
    Fixes apps showing redundant error messages and terminating
    unnecessarily on failed file requests. It's nicer to drop the
    user off at the equivalent of a default document on failure if
    possible.
    
    Also fixes TextEditor not showing response errors for missing files
    in the recently opened list.
```
