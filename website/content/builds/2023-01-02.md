---
title: 'SerenityOS build: Monday, January 02'
date: 2023-01-02T05:09:43Z
author: Buildbot
description: |
  Commit: b8e4e473e8 (Base: Add 8 new emoji and improve consistency on one more, 2023-01-01)

  - [serenity-x86_64-20230102-b8e4e47.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230102-b8e4e47.img.gz) (Raw image, 654.38 MiB)
  - [serenity-x86_64-20230102-b8e4e47.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230102-b8e4e47.vdi.gz) (VirtualBox VDI, 654.09 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230102-b8e4e47.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230102-b8e4e47.img.gz) (Raw image, 654.38 MiB)
- [serenity-x86_64-20230102-b8e4e47.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230102-b8e4e47.vdi.gz) (VirtualBox VDI, 654.09 MiB)

### Last commit :star:

```git
commit b8e4e473e8f65bfbfe53fee56c642ba88a45ef03
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Sun Jan 1 23:47:04 2023 +0100
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Jan 1 18:42:29 2023 -0500

    Base: Add 8 new emoji and improve consistency on one more
    
    - 👐 U+1F450 Open Hands
    - 🙌 U+1F64C Raising Hands is modified to match 👐 and other hand emoji.
    - ✊ U+270A Raised Fist
    - ✴️ U+2734 Eight-Pointed Star
    - ❇️ U+2747 Sparkle
    - ❎ U+274E Cross Mark Button
    - ❤️‍🔥 U+2764 U+200D U+1F525 Heart on Fire
    - 〰️ U+3030 Wavy Dash
```
