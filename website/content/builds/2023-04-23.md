---
title: 'SerenityOS build: Sunday, April 23'
date: 2023-04-23T05:03:30Z
author: Buildbot
description: |
  Commit: 16ef5e2631 (Ports: Prefer host python3.xx over python3, 2023-04-21)

  - [serenity-x86_64-20230423-16ef5e2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230423-16ef5e2.img.gz) (Raw image, 179.93 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230423-16ef5e2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230423-16ef5e2.img.gz) (Raw image, 179.93 MiB)

### Last commit :star:

```git
commit 16ef5e26310f6c5239993ffdd0f63892e1d14956
Author:     Fabian Dellwing <fabian.dellwing@gmail.com>
AuthorDate: Fri Apr 21 20:24:10 2023 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Apr 22 12:56:36 2023 +0100

    Ports: Prefer host python3.xx over python3
    
    Previously we relied on the presence of a `python3` binary in the
    PATH that has the correct minor version to build the port.
    
    We now first check for the presence of a `python3.minor` binary
    in the PATH and use that if found.
    
    This allows users that have multiple Python versions installed
    simultaneously (like from a PPA) to build the port without having
    to change their main version.
```
