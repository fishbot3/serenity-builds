---
title: 'SerenityOS build: Friday, April 12'
date: 2024-04-12T05:17:37Z
author: Buildbot
description: |
  Commit: ee3dd7977d (LibWeb: Add popstate event support, 2024-04-10)

  - [serenity-x86_64-20240412-ee3dd79.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240412-ee3dd79.img.gz) (Raw image, 221.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240412-ee3dd79.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240412-ee3dd79.img.gz) (Raw image, 221.73 MiB)

### Last commit :star:

```git
commit ee3dd7977d4c88c836bfb813adcf3e006da749a8
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Wed Apr 10 21:25:31 2024 -0700
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Apr 11 21:25:06 2024 +0200

    LibWeb: Add popstate event support
    
    It is going to be useful in writing tests for History API.
```
