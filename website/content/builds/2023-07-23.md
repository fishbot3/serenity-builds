---
title: 'SerenityOS build: Sunday, July 23'
date: 2023-07-23T05:06:50Z
author: Buildbot
description: |
  Commit: 77e6dbab33 (LibPDF: Fix symbol for text_next_line_show_string_set_spacing operator, 2023-07-22)

  - [serenity-x86_64-20230723-77e6dba.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230723-77e6dba.img.gz) (Raw image, 187.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230723-77e6dba.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230723-77e6dba.img.gz) (Raw image, 187.58 MiB)

### Last commit :star:

```git
commit 77e6dbab333507eb34a5afa991d0afe68b13d7da
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Jul 22 11:11:53 2023 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Jul 22 12:25:30 2023 -0400

    LibPDF: Fix symbol for text_next_line_show_string_set_spacing operator
    
    It's `"`, not `''`.
    
    Now the `text_next_line_show_string_set_spacing` gets called and logs
    a TODO at page render time if `"` is used in a PDF:
    
        warning: Rendering of feature not supported:
            draw operation: text_next_line_show_string_set_spacing
    
    It caused a parse error (also at page render time) previously:
    
        [parse_value @ .../LibPDF/Parser.cpp:104]
            Parser error at offset 611: Unexpected char """
```
