---
title: 'SerenityOS build: Tuesday, May 31'
date: 2022-05-31T03:05:34Z
author: Buildbot
description: |
  Commit: df8df947f6 (Kernel: Do not include AK/Platform.h in mcontext headers, 2022-05-29)

  - [serenity-i686-20220531-df8df94.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220531-df8df94.img.gz) (Raw image, 123.35 MiB)
  - [serenity-i686-20220531-df8df94.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220531-df8df94.vdi.gz) (VirtualBox VDI, 123.16 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220531-df8df94.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220531-df8df94.img.gz) (Raw image, 123.35 MiB)
- [serenity-i686-20220531-df8df94.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220531-df8df94.vdi.gz) (VirtualBox VDI, 123.16 MiB)

### Last commit :star:

```git
commit df8df947f64bc3e240c47d81b19476d40b0e2b18
Author:     SeekingBlues <seekingblues@gmail.com>
AuthorDate: Sun May 29 22:51:34 2022 -0500
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon May 30 21:39:41 2022 +0200

    Kernel: Do not include AK/Platform.h in mcontext headers
    
    Including signal.h would cause several ports to fail on build,
    because it would end up including AK/Platform.h through these
    mcontext headers. This is problematic because AK/Platform.h defines
    several macros with very common names, such as `NAKED` (breaks radare2),
    and `NO_SANITIZE_ADDRESS` and `ALWAYS_INLINE` (breaks ruby).
```
