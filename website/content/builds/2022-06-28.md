---
title: 'SerenityOS build: Tuesday, June 28'
date: 2022-06-28T03:04:37Z
author: Buildbot
description: |
  Commit: 50642f85ac (LibWeb/WebGL: Add WebGLRenderingContextBase.canvas, 2022-06-19)

  - [serenity-i686-20220628-50642f8.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220628-50642f8.img.gz) (Raw image, 124.8 MiB)
  - [serenity-i686-20220628-50642f8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220628-50642f8.vdi.gz) (VirtualBox VDI, 124.62 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220628-50642f8.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220628-50642f8.img.gz) (Raw image, 124.8 MiB)
- [serenity-i686-20220628-50642f8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220628-50642f8.vdi.gz) (VirtualBox VDI, 124.62 MiB)

### Last commit :star:

```git
commit 50642f85ac547a3caee353affcb08872cac49456
Author:     Luke Wilde <lukew@serenityos.org>
AuthorDate: Sun Jun 19 20:11:10 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Jun 27 22:54:41 2022 +0100

    LibWeb/WebGL: Add WebGLRenderingContextBase.canvas
```
