---
title: 'SerenityOS build: Friday, July 22'
date: 2022-07-22T03:05:45Z
author: Buildbot
description: |
  Commit: b8d4f8debf (LibJS: Selectively display DateTimeFormat day periods as noon, 2022-07-21)

  - [serenity-i686-20220722-b8d4f8d.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220722-b8d4f8d.vdi.gz) (VirtualBox VDI, 127.51 MiB)
  - [serenity-i686-20220722-b8d4f8d.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220722-b8d4f8d.img.gz) (Raw image, 127.69 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220722-b8d4f8d.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220722-b8d4f8d.vdi.gz) (VirtualBox VDI, 127.51 MiB)
- [serenity-i686-20220722-b8d4f8d.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220722-b8d4f8d.img.gz) (Raw image, 127.69 MiB)

### Last commit :star:

```git
commit b8d4f8debf46e9591102bf8d609b4f32cab6b3f3
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Thu Jul 21 14:41:47 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu Jul 21 20:36:03 2022 +0100

    LibJS: Selectively display DateTimeFormat day periods as noon
```
