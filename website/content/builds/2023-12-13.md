---
title: 'SerenityOS build: Wednesday, December 13'
date: 2023-12-13T06:12:57Z
author: Buildbot
description: |
  Commit: 41a3c19cfe (LibWeb: Make HTML::DecodedImageData to be GC-allocated, 2023-12-12)

  - [serenity-x86_64-20231213-41a3c19.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231213-41a3c19.img.gz) (Raw image, 206.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231213-41a3c19.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231213-41a3c19.img.gz) (Raw image, 206.37 MiB)

### Last commit :star:

```git
commit 41a3c19cfecd490fb2a14a8365d8ad051c604669
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Tue Dec 12 20:07:19 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Dec 12 23:01:08 2023 +0100

    LibWeb: Make HTML::DecodedImageData to be GC-allocated
    
    This change fixes GC-leak caused by following mutual dependency:
    - SVGDecodedImageData owns JS::Handle for Page.
    - SVGDecodedImageData is owned by visited objects.
    by making everything inherited from HTML::DecodedImageData and
    ListOfAvailableImages to be GC-allocated.
    
    Generally, if visited object has a handle, very likely we leak
    everything visited from object in a handle.
```
