---
title: 'SerenityOS build: Monday, January 08'
date: 2024-01-08T06:11:39Z
author: Buildbot
description: |
  Commit: 5c02b960ab (LibGfx: Take into account unicode ranges to find font for space glyph, 2024-01-08)

  - [serenity-x86_64-20240108-5c02b96.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240108-5c02b96.img.gz) (Raw image, 207.15 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240108-5c02b96.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240108-5c02b96.img.gz) (Raw image, 207.15 MiB)

### Last commit :star:

```git
commit 5c02b960aba9b16a8d0fb251ad0477995efefbe9
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Mon Jan 8 00:10:10 2024 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Jan 8 01:00:24 2024 +0100

    LibGfx: Take into account unicode ranges to find font for space glyph
    
    Instead of assuming that the first font in the cascade font list will
    have a glyph for space, we need to find it in the list taking into
    account unicode ranges.
```
