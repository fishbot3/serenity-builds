---
title: 'SerenityOS build: Tuesday, June 13'
date: 2023-06-13T05:04:33Z
author: Buildbot
description: |
  Commit: 521ad55a61 (SystemServer: Handle `waitpid`'s status correctly, 2023-05-06)

  - [serenity-x86_64-20230613-521ad55.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230613-521ad55.img.gz) (Raw image, 184.16 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230613-521ad55.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230613-521ad55.img.gz) (Raw image, 184.16 MiB)

### Last commit :star:

```git
commit 521ad55a61cbdf915acd179ba4eea385663bdddb
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Sat May 6 00:41:01 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Jun 13 06:15:10 2023 +0200

    SystemServer: Handle `waitpid`'s status correctly
    
    We used to call `did_exit()` directly with the status returned from
    `waitpid` but the function expected an exit code. We now use several
    of `wait`-related macros to deduce the correct information.
```
