---
title: 'SerenityOS build: Wednesday, March 29'
date: 2023-03-29T05:02:40Z
author: Buildbot
description: |
  Commit: d01ac59b82 (Shell: Skip rc files when not running interactively, 2023-03-23)

  - [serenity-x86_64-20230329-d01ac59.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230329-d01ac59.img.gz) (Raw image, 169.03 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230329-d01ac59.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230329-d01ac59.img.gz) (Raw image, 169.03 MiB)

### Last commit :star:

```git
commit d01ac59b824244c9927673258f7da522d153e932
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Thu Mar 23 16:15:01 2023 +0100
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Wed Mar 29 03:39:09 2023 +0330

    Shell: Skip rc files when not running interactively
    
    This makes debugging a bit nicer.
```
