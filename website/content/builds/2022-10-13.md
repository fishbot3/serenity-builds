---
title: 'SerenityOS build: Thursday, October 13'
date: 2022-10-13T05:38:35Z
author: Buildbot
description: |
  Commit: d7d50d6d9e (AK: Fix FreeBSD compilation for clock, 2022-10-13)

  - [serenity-x86_64-20221013-d7d50d6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221013-d7d50d6.vdi.gz) (VirtualBox VDI, 135.29 MiB)
  - [serenity-x86_64-20221013-d7d50d6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221013-d7d50d6.img.gz) (Raw image, 135.65 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221013-d7d50d6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221013-d7d50d6.vdi.gz) (VirtualBox VDI, 135.29 MiB)
- [serenity-x86_64-20221013-d7d50d6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221013-d7d50d6.img.gz) (Raw image, 135.65 MiB)

### Last commit :star:

```git
commit d7d50d6d9e60ca692b212f5ee0b6a173b6f60579
Author:     Al Hoang <13622+hoanga@users.noreply.github.com>
AuthorDate: Thu Oct 13 03:10:57 2022 +0000
Commit:     Andrew Kaster <andrewdkaster@gmail.com>
CommitDate: Wed Oct 12 23:12:13 2022 -0600

    AK: Fix FreeBSD compilation for clock
    
    FreeBSD introduced CLOCK_MONOTONIC_COARSE and CLOCK_REALTIME_COARSE.
    This update fixes ladybird builds from FreeBSD 13.1
    
    see clock_gettime(2) https://www.freebsd.org/cgi/man.cgi?query=clock_gettime&apropos=0&sektion=0&manpath=FreeBSD+13.1-RELEASE&arch=default&format=ascii
```
