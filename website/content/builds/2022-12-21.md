---
title: 'SerenityOS build: Wednesday, December 21'
date: 2022-12-21T04:11:11Z
author: Buildbot
description: |
  Commit: ed84a6f6ee (LibUnicode: Use www.unicode.org domain to download emoji-test.txt, 2022-12-20)

  - [serenity-x86_64-20221221-ed84a6f.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221221-ed84a6f.vdi.gz) (VirtualBox VDI, 144.5 MiB)
  - [serenity-x86_64-20221221-ed84a6f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221221-ed84a6f.img.gz) (Raw image, 144.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221221-ed84a6f.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221221-ed84a6f.vdi.gz) (VirtualBox VDI, 144.5 MiB)
- [serenity-x86_64-20221221-ed84a6f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221221-ed84a6f.img.gz) (Raw image, 144.81 MiB)

### Last commit :star:

```git
commit ed84a6f6ee4a620e936c53f91407cd04fe2b7106
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Tue Dec 20 06:47:27 2022 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Tue Dec 20 12:09:16 2022 -0500

    LibUnicode: Use www.unicode.org domain to download emoji-test.txt
    
    The non-www domain does not appear to be available now. We use the www
    domain for UCD.zip already.
    
    Co-authored-by: Stephan Unverwerth <s.unverwerth@serenityos.org>
```
