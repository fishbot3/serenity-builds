---
title: 'SerenityOS build: Saturday, September 02'
date: 2023-09-02T05:08:00Z
author: Buildbot
description: |
  Commit: 720f7ba746 (LibWeb: Define getting and setting an attribute value, 2023-09-01)

  - [serenity-x86_64-20230902-720f7ba.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230902-720f7ba.img.gz) (Raw image, 186.22 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230902-720f7ba.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230902-720f7ba.img.gz) (Raw image, 186.22 MiB)

### Last commit :star:

```git
commit 720f7ba7463c67fe205cedfce7bde6a6ba3b14c8
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Sep 1 10:42:20 2023 -0400
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Sat Sep 2 01:46:37 2023 +0330

    LibWeb: Define getting and setting an attribute value
```
