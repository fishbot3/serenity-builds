---
title: 'SerenityOS build: Sunday, April 16'
date: 2023-04-16T05:03:06Z
author: Buildbot
description: |
  Commit: f7ac121ac4 (Meta: Allow overriding the calculated disk image inode count, 2023-04-15)

  - [serenity-x86_64-20230416-f7ac121.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230416-f7ac121.img.gz) (Raw image, 178.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230416-f7ac121.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230416-f7ac121.img.gz) (Raw image, 178.81 MiB)

### Last commit :star:

```git
commit f7ac121ac41d5f59c1835a6a8261341c37423846
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Sat Apr 15 23:04:25 2023 +0200
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Sat Apr 15 19:41:08 2023 -0700

    Meta: Allow overriding the calculated disk image inode count
```
