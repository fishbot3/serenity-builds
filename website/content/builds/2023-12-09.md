---
title: 'SerenityOS build: Saturday, December 09'
date: 2023-12-09T06:10:25Z
author: Buildbot
description: |
  Commit: 9ab312e001 (LibWeb: Hide XHR send debug messages behind SPAM_DEBUG, 2023-12-08)

  - [serenity-x86_64-20231209-9ab312e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231209-9ab312e.img.gz) (Raw image, 205.56 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231209-9ab312e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231209-9ab312e.img.gz) (Raw image, 205.56 MiB)

### Last commit :star:

```git
commit 9ab312e00170a5db8d54fe2da41195aebe3d1c48
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Fri Dec 8 14:09:09 2023 -0700
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Dec 8 20:04:13 2023 -0500

    LibWeb: Hide XHR send debug messages behind SPAM_DEBUG
```
