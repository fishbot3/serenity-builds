---
title: 'SerenityOS build: Sunday, August 14'
date: 2022-08-14T03:06:01Z
author: Buildbot
description: |
  Commit: 2121760700 (WorkspacePicker: Allow opening workspace settings via a context menu, 2022-08-13)

  - [serenity-i686-20220814-2121760.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220814-2121760.img.gz) (Raw image, 127.88 MiB)
  - [serenity-i686-20220814-2121760.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220814-2121760.vdi.gz) (VirtualBox VDI, 127.69 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220814-2121760.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220814-2121760.img.gz) (Raw image, 127.88 MiB)
- [serenity-i686-20220814-2121760.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220814-2121760.vdi.gz) (VirtualBox VDI, 127.69 MiB)

### Last commit :star:

```git
commit 21217607006d7b316ae15b022335571234a63cd6
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Sat Aug 13 21:04:11 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Aug 14 02:28:25 2022 +0200

    WorkspacePicker: Allow opening workspace settings via a context menu
    
    Previously you had to open Display Settings and navigate to the
    "Workspaces" tab in order to edit workspace settings. This patch adds a
    context menu shortcut to the same place.
```
