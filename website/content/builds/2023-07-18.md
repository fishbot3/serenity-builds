---
title: 'SerenityOS build: Tuesday, July 18'
date: 2023-07-18T05:06:51Z
author: Buildbot
description: |
  Commit: ac124fbaae (LibWeb: Resolve flex item percentages against used flex container sizes, 2023-07-17)

  - [serenity-x86_64-20230718-ac124fb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230718-ac124fb.img.gz) (Raw image, 187.94 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230718-ac124fb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230718-ac124fb.img.gz) (Raw image, 187.94 MiB)

### Last commit :star:

```git
commit ac124fbaae7732e733f21f5a550840e620ecfb8d
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Mon Jul 17 21:19:33 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Jul 18 06:04:55 2023 +0200

    LibWeb: Resolve flex item percentages against used flex container sizes
    
    Once we've resolved the used flex item width & height, we should allow
    percentage flex item sizes to resolve against them instead of forcing
    flex items to always treat percentages as auto while doing intrinsic
    sizing layout.
    
    Regressed in 8dd489da61362738a39ec938732e3d054ff5da8b.
```
