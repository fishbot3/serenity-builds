---
title: 'SerenityOS build: Monday, April 22'
date: 2024-04-22T05:17:16Z
author: Buildbot
description: |
  Commit: 511e411def (Kernel/riscv64: Implement Processor::read_cpu_counter, 2024-04-21)

  - [serenity-x86_64-20240422-511e411.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240422-511e411.img.gz) (Raw image, 220.77 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240422-511e411.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240422-511e411.img.gz) (Raw image, 220.77 MiB)

### Last commit :star:

```git
commit 511e411def7b0b1ef3cff21511ccb180729bfcbb
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sun Apr 21 12:55:36 2024 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Apr 21 13:37:32 2024 -0600

    Kernel/riscv64: Implement Processor::read_cpu_counter
    
    This simply reads the current cycle count from the cycle CSR.
    x86-64 uses the similar rdtsc instruction here, which also may or may
    not tick at a constant rate.
```
