---
title: 'SerenityOS build: Saturday, May 06'
date: 2023-05-06T05:04:26Z
author: Buildbot
description: |
  Commit: 4c17f22735 (Ports: Unbreak SDL2 by using GUI::Application::create(), 2023-05-05)

  - [serenity-x86_64-20230506-4c17f22.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230506-4c17f22.img.gz) (Raw image, 181.11 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230506-4c17f22.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230506-4c17f22.img.gz) (Raw image, 181.11 MiB)

### Last commit :star:

```git
commit 4c17f2273568a2220b3aae60a6dfc476fc15b814
Author:     Kenneth Myhra <kennethmyhra@serenityos.org>
AuthorDate: Fri May 5 22:50:18 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sat May 6 00:08:59 2023 +0200

    Ports: Unbreak SDL2 by using GUI::Application::create()
    
    Commit 1a97382 introduced the fallible GUI::Application::create() and
    removed GUI::Application::construct() breaking the SDL2 port, let's
    update it to use the fallible version.
```
