---
title: 'SerenityOS build: Wednesday, July 26'
date: 2023-07-26T05:07:30Z
author: Buildbot
description: |
  Commit: 6266976e7a (LibTLS: Move singleton for DefaultRootCACertificates out of line, 2023-07-24)

  - [serenity-x86_64-20230726-6266976.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230726-6266976.img.gz) (Raw image, 187.89 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230726-6266976.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230726-6266976.img.gz) (Raw image, 187.89 MiB)

### Last commit :star:

```git
commit 6266976e7a88046ebd2afc41fac19aa6e6b491cc
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Mon Jul 24 17:48:34 2023 -0600
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jul 26 05:34:38 2023 +0200

    LibTLS: Move singleton for DefaultRootCACertificates out of line
    
    This follows the pattern of every other singleton in the system.
    
    Also, remove use of AK::Singleton in place of a function-scope static.
    There are only three uses of that class outside of the Kernel, and all
    the remaining uses are suspect. We need it in the Kernel because we
    want to avoid global destructors to prevent nasty surprises about
    expected lifetimes of objects. In Userland, we have normal thread-safe
    statics available. 7d11edbe1 attempted to standardize the pattern, but
    it seems like more uses of awkward singleton creation have crept in or
    were missed back then.
    
    As a bonus, this fixes a linker error on macOS with -g -O0 for Lagom
    WebContent.
```
