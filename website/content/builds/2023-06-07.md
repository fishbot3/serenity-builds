---
title: 'SerenityOS build: Wednesday, June 07'
date: 2023-06-07T05:03:54Z
author: Buildbot
description: |
  Commit: 0c2fcffba3 (LibWeb: Make text-shadow inherited in CSS::ComputedValues, 2023-06-06)

  - [serenity-x86_64-20230607-0c2fcff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230607-0c2fcff.img.gz) (Raw image, 183.65 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230607-0c2fcff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230607-0c2fcff.img.gz) (Raw image, 183.65 MiB)

### Last commit :star:

```git
commit 0c2fcffba3583931ae79eb5b7aa21cf5c1440528
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Tue Jun 6 21:09:32 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jun 7 06:29:59 2023 +0200

    LibWeb: Make text-shadow inherited in CSS::ComputedValues
    
    CSS text-shadow is an inherited property, so we have to make sure it's
    part of the inherited substructure in ComputedValues, otherwise it gets
    incorrectly reset in children.
```
